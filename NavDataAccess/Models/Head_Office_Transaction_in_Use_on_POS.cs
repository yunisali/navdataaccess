//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace NavDataAccess.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Head_Office_Transaction_in_Use_on_POS
    {
        public byte[] timestamp { get; set; }
        public string Receipt_No_ { get; set; }
        public string In_Use_on_POS_Terminal { get; set; }
        public string Staff_ID { get; set; }
        public string User_ID { get; set; }
        public System.DateTime Date_Time_Set { get; set; }
        public System.DateTime Date_Time_Released { get; set; }
    }
}
