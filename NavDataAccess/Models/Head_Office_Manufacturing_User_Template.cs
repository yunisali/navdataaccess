//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace NavDataAccess.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Head_Office_Manufacturing_User_Template
    {
        public byte[] timestamp { get; set; }
        public string User_ID { get; set; }
        public int Create_Purchase_Order { get; set; }
        public int Create_Production_Order { get; set; }
        public int Create_Transfer_Order { get; set; }
        public int Create_Assembly_Order { get; set; }
        public string Purchase_Req__Wksh__Template { get; set; }
        public string Purchase_Wksh__Name { get; set; }
        public string Prod__Req__Wksh__Template { get; set; }
        public string Prod__Wksh__Name { get; set; }
        public string Transfer_Req__Wksh__Template { get; set; }
        public string Transfer_Wksh__Name { get; set; }
        public int Make_Orders { get; set; }
    }
}
