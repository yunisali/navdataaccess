//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace NavDataAccess.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Head_Office_Safe_Bag_Line
    {
        public byte[] timestamp { get; set; }
        public string Safe_No_ { get; set; }
        public string Bag_No_ { get; set; }
        public int Type { get; set; }
        public decimal Amount { get; set; }
        public int Version { get; set; }
        public string Tender_Type { get; set; }
        public int Qty_ { get; set; }
        public decimal Total { get; set; }
        public string Description { get; set; }
        public byte Active_Version { get; set; }
        public byte Changed_From_Original { get; set; }
        public int Safe_Entry_No_ { get; set; }
        public string Store_No_ { get; set; }
    }
}
