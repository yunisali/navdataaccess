﻿using RollbarSharp;
using ServiceStack.OrmLite;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NavDataAccess
{
    //class TestData
    //{
    //    public int Id { get; set; }
    //    public string Name { get; set; }
    //}

    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                throw new Exception("rollbar test exception");

                //var connectionString = ConfigurationManager.ConnectionStrings["DefaultConnectionString"].ConnectionString;

                //var dbFactory = new OrmLiteConnectionFactory(connectionString, SqlServerDialect.Provider);

                //using (var db = dbFactory.Open())
                //{
                //    using (var tran = db.OpenTransaction())
                //    {
                //        if (db.CreateTableIfNotExists<TestData>())
                //        {
                //            db.Insert(new TestData { Id = 1, Name = "Seed Data" });
                //        }
                //        //db.Insert(new TestData { Id = 2, Name = "Seed Data" });
                //        //tran.Commit();

                //        var res = db.Select<dynamic>("select top 10 * from [dbo].[Head Office$Customer]");
                //        var result = db.SingleById<TestData>(1);
                //        Console.WriteLine("Read: " + result.Id + " " + result.Name); //= {Id: 1, Name:Seed Data}
                //    }
                //}

                var mod = new Models.NAV_DEVEntities();
                var ds = mod.Head_Office_Access_Department.AsEnumerable();
                foreach (var d in ds)
                {
                    Console.WriteLine(d.Code + " " + d.Name);
                }
            }
            catch(Exception e)
            {
                (new RollbarClient()).SendException(e);
            }

            Console.Read();
        }
    }
}
