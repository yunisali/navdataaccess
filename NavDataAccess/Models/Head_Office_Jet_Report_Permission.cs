//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace NavDataAccess.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Head_Office_Jet_Report_Permission
    {
        public byte[] timestamp { get; set; }
        public int Header_ID { get; set; }
        public string RangeName { get; set; }
        public string Contact_No_ { get; set; }
        public string Param_Filter { get; set; }
        public string Override_Value { get; set; }
    }
}
