//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace NavDataAccess.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Head_Office_Acc__Sched__KPI_Web_Srv__Setup
    {
        public byte[] timestamp { get; set; }
        public string Primary_Key { get; set; }
        public int Forecasted_Values_Start { get; set; }
        public string G_L_Budget_Name { get; set; }
        public int Period { get; set; }
        public int View_By { get; set; }
        public string Web_Service_Name { get; set; }
    }
}
