//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace NavDataAccess.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Head_Office_Item_Analysis_View_Filter
    {
        public byte[] timestamp { get; set; }
        public int Analysis_Area { get; set; }
        public string Analysis_View_Code { get; set; }
        public string Dimension_Code { get; set; }
        public string Dimension_Value_Filter { get; set; }
    }
}
