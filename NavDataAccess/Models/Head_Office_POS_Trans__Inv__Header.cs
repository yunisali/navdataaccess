//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace NavDataAccess.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Head_Office_POS_Trans__Inv__Header
    {
        public byte[] timestamp { get; set; }
        public string Store_No_ { get; set; }
        public string POS_Terminal_No_ { get; set; }
        public string Transaction_No_ { get; set; }
        public string Receipt_No_ { get; set; }
        public int Transaction_Type { get; set; }
        public string Staff_ID { get; set; }
        public System.DateTime Date { get; set; }
        public System.DateTime Time { get; set; }
        public string Shift_No_ { get; set; }
        public System.DateTime Shift_Date { get; set; }
        public decimal No__of_Items { get; set; }
        public int Entry_Status { get; set; }
        public decimal Total_Amount { get; set; }
        public decimal Total_Qty { get; set; }
        public decimal Qty_Left { get; set; }
        public System.DateTime Start_Process { get; set; }
        public System.DateTime End_Process { get; set; }
        public int Mask_No { get; set; }
        public string Mask_Description { get; set; }
        public int Mask_Type { get; set; }
        public string Document_Member { get; set; }
        public int Document_Type { get; set; }
        public string Document_No_ { get; set; }
        public string Document_Member_Name { get; set; }
        public string P_R_Counting_Header_No { get; set; }
        public System.DateTime Expected_Date { get; set; }
        public int No__Of_Check_Rounds { get; set; }
        public int Quantity_Method { get; set; }
        public decimal Quick_default_Quantity { get; set; }
        public byte Replicated { get; set; }
        public int Replication_Counter { get; set; }
    }
}
