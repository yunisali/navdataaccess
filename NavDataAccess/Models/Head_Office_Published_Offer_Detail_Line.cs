//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace NavDataAccess.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Head_Office_Published_Offer_Detail_Line
    {
        public byte[] timestamp { get; set; }
        public string Offer_No_ { get; set; }
        public int Line_No_ { get; set; }
        public string Description { get; set; }
        public byte[] Picture { get; set; }
    }
}
