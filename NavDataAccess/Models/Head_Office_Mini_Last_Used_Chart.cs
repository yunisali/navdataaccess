//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace NavDataAccess.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Head_Office_Mini_Last_Used_Chart
    {
        public byte[] timestamp { get; set; }
        public string UID { get; set; }
        public int Code_Unit_ID { get; set; }
        public string Chart_Name { get; set; }
    }
}
