//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace NavDataAccess.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Head_Office_Trans__Add__Salesperson
    {
        public byte[] timestamp { get; set; }
        public string Store_No_ { get; set; }
        public string POS_Terminal_No_ { get; set; }
        public int Transaction_No_ { get; set; }
        public int Line_No_ { get; set; }
        public string Staff_ID { get; set; }
        public System.DateTime Date { get; set; }
        public System.DateTime Time { get; set; }
        public byte Replicated { get; set; }
        public int Replication_Counter { get; set; }
    }
}
