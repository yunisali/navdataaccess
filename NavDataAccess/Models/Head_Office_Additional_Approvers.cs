//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace NavDataAccess.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Head_Office_Additional_Approvers
    {
        public byte[] timestamp { get; set; }
        public string Approver_ID { get; set; }
        public string Approval_Code { get; set; }
        public int Approval_Type { get; set; }
        public int Document_Type { get; set; }
        public int Limit_Type { get; set; }
        public int Sequence_No_ { get; set; }
    }
}
