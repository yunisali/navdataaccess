//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace NavDataAccess.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Head_Office_Vendor_Item_Library
    {
        public byte[] timestamp { get; set; }
        public string Item_No_ { get; set; }
        public string Vendor_No_ { get; set; }
        public string Vendor_Item_No_ { get; set; }
        public string Base_Unit_of_Measure { get; set; }
        public string Description { get; set; }
        public string Description_2 { get; set; }
        public string Division_Code { get; set; }
        public string Item_Category_Code { get; set; }
        public string Product_Group_Code { get; set; }
        public decimal Unit_Price_Excl__VAT { get; set; }
        public decimal Unit_Price_Incl__VAT { get; set; }
        public decimal Unit_Cost { get; set; }
        public string VAT_Prod__Posting_Group { get; set; }
        public string Gen__Prod__Posting_Group { get; set; }
        public string Inventory_Posting_Group { get; set; }
        public string No__Series { get; set; }
        public string Item_Family_Code { get; set; }
        public byte Available_for_Use { get; set; }
        public string Item_Tracking_Code { get; set; }
        public byte Multiple_Selection { get; set; }
        public int Min__Selection__For_Future_Use { get; set; }
        public int Max__Selection_For_Future_Use { get; set; }
        public string Explanatory_Header_Text { get; set; }
        public int When_OK_Pressed { get; set; }
        public string VAT_Bus__Posting_Gr___Price_ { get; set; }
        public byte Item_Created { get; set; }
    }
}
