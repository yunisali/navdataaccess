//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace NavDataAccess.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Head_Office_POS_Browser_Control
    {
        public byte[] timestamp { get; set; }
        public string Interface_Profile_ID { get; set; }
        public string Control_ID { get; set; }
        public string Description { get; set; }
        public string URL { get; set; }
        public string Parent_Panel_ID { get; set; }
        public byte Show_Border { get; set; }
        public int Border_Width { get; set; }
        public string Border_Color { get; set; }
        public string Border_Color_2 { get; set; }
        public int Border_Dash_Style { get; set; }
        public int Border_Pattern_Style { get; set; }
        public int Padding { get; set; }
        public string Context_Menu { get; set; }
        public string POS_Module { get; set; }
    }
}
