//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace NavDataAccess.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Head_Office_Purchase_Contract_Lines
    {
        public byte[] timestamp { get; set; }
        public string Purchase_Contract_No_ { get; set; }
        public int Line_No_ { get; set; }
        public int Type { get; set; }
        public string Item_Category_Code { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public decimal Goal_Amount { get; set; }
        public decimal Discount__ { get; set; }
        public decimal Purchase_Amount_LCY__Internal_ { get; set; }
        public decimal Purchase_Disc__LCY__Internal_ { get; set; }
    }
}
