//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace NavDataAccess.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Head_Office_Requisition_Line_VSIFT_7
    {
        public int Replenishment_System { get; set; }
        public int Type { get; set; }
        public string No_ { get; set; }
        public string Variant_Code { get; set; }
        public string Transfer_from_Code { get; set; }
        public System.DateTime Transfer_Shipment_Date { get; set; }
        public Nullable<long> C_Cnt { get; set; }
        public Nullable<decimal> SUM_Quantity__Base_ { get; set; }
    }
}
