//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace NavDataAccess.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Head_Office_Post_Code
    {
        public byte[] timestamp { get; set; }
        public string Code { get; set; }
        public string City { get; set; }
        public string Search_City { get; set; }
        public string Country_Region_Code { get; set; }
        public string County { get; set; }
    }
}
