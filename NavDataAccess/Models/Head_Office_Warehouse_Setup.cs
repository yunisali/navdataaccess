//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace NavDataAccess.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Head_Office_Warehouse_Setup
    {
        public byte[] timestamp { get; set; }
        public string Primary_Key { get; set; }
        public string Whse__Receipt_Nos_ { get; set; }
        public string Whse__Put_away_Nos_ { get; set; }
        public string Whse__Pick_Nos_ { get; set; }
        public string Whse__Ship_Nos_ { get; set; }
        public string Registered_Whse__Pick_Nos_ { get; set; }
        public string Registered_Whse__Put_away_Nos_ { get; set; }
        public byte Require_Receive { get; set; }
        public byte Require_Put_away { get; set; }
        public byte Require_Pick { get; set; }
        public byte Require_Shipment { get; set; }
        public int Last_Whse__Posting_Ref__No_ { get; set; }
        public int Receipt_Posting_Policy { get; set; }
        public int Shipment_Posting_Policy { get; set; }
        public string Posted_Whse__Receipt_Nos_ { get; set; }
        public string Posted_Whse__Shipment_Nos_ { get; set; }
        public string Whse__Internal_Put_away_Nos_ { get; set; }
        public string Whse__Internal_Pick_Nos_ { get; set; }
        public string Whse__Movement_Nos_ { get; set; }
        public string Registered_Whse__Movement_Nos_ { get; set; }
        public byte Confirm_Cycle_Count { get; set; }
        public byte Post_Cycle_Count_Item_Journal { get; set; }
        public byte Use_Virtual_Bins { get; set; }
        public byte Use_Virtual_IBTs { get; set; }
        public decimal Pick_Face__ { get; set; }
        public string Pick_Face_Zone_Filter { get; set; }
        public string Bulk_Zone_Filter { get; set; }
        public byte Consignment_Tracking { get; set; }
        public string Consignment_Nos_ { get; set; }
        public string Web_Return_Store { get; set; }
        public string Web_Return_Till { get; set; }
        public decimal Bulk_Put_Away_Threshold { get; set; }
        public string Disc__Whse__Journal_Batch { get; set; }
    }
}
