//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace NavDataAccess.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Head_Office_Scd_Subjob_Linked_Table_Filter
    {
        public byte[] timestamp { get; set; }
        public string Subjob_ID { get; set; }
        public int Linked_Table_ID { get; set; }
        public int Linked_Field_ID { get; set; }
        public int Link_Type { get; set; }
        public int Field_No_ { get; set; }
        public string Filter { get; set; }
    }
}
