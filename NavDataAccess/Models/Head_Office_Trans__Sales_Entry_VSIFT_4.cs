//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace NavDataAccess.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Head_Office_Trans__Sales_Entry_VSIFT_4
    {
        public string Store_No_ { get; set; }
        public string Section { get; set; }
        public string Shelf { get; set; }
        public System.DateTime Date { get; set; }
        public Nullable<long> C_Cnt { get; set; }
        public Nullable<decimal> SUM_Quantity { get; set; }
        public Nullable<decimal> SUM_Net_Amount { get; set; }
        public Nullable<decimal> SUM_Discount_Amount { get; set; }
        public Nullable<decimal> SUM_Cost_Amount { get; set; }
        public Nullable<decimal> SUM_VAT_Amount { get; set; }
    }
}
