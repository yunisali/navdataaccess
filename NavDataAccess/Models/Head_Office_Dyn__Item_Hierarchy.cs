//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace NavDataAccess.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Head_Office_Dyn__Item_Hierarchy
    {
        public byte[] timestamp { get; set; }
        public string Hierarchy_Setup_ID { get; set; }
        public int Line_No_ { get; set; }
        public int Level { get; set; }
        public byte Bold { get; set; }
        public int Type { get; set; }
        public string Member_Of { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public int Link_Table_No_ { get; set; }
        public int Primary_Field_No_ { get; set; }
        public int Item_Field_No_ { get; set; }
        public byte Switch_To_Item { get; set; }
        public int Calc__Field1_No_ { get; set; }
        public int Calc__Field2_No_ { get; set; }
        public int Calc__Field3_No_ { get; set; }
        public int Calc__Field4_No_ { get; set; }
        public int Calc__Field5_No_ { get; set; }
        public decimal Calc__Field1 { get; set; }
        public decimal Calc__Field2 { get; set; }
        public decimal Calc__Field3 { get; set; }
        public decimal Calc__Field4 { get; set; }
        public decimal Calc__Field5 { get; set; }
        public int Location_Filter_Field_No_ { get; set; }
        public int Date_Filter_Field_No_ { get; set; }
        public int Global_Dim_2_Filter_Field_No_ { get; set; }
        public string Calc__Field1_Formula { get; set; }
        public string Calc__Field2_Formula { get; set; }
        public string Calc__Field3_Formula { get; set; }
        public string Calc__Field4_Formula { get; set; }
        public string Calc__Field5_Formula { get; set; }
        public string Calc__Field1_Caption { get; set; }
        public string Calc__Field2_Caption { get; set; }
        public string Calc__Field3_Caption { get; set; }
        public string Calc__Field4_Caption { get; set; }
        public string Calc__Field5_Caption { get; set; }
        public byte Reverse_Sign_Field1 { get; set; }
        public byte Reverse_Sign_Field2 { get; set; }
        public byte Reverse_Sign_Field3 { get; set; }
        public byte Reverse_Sign_Field4 { get; set; }
        public byte Reverse_Sign_Field5 { get; set; }
        public byte Show_Line { get; set; }
        public string ExpandChar { get; set; }
    }
}
