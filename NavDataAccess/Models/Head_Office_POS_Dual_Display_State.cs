//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace NavDataAccess.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Head_Office_POS_Dual_Display_State
    {
        public byte[] timestamp { get; set; }
        public string DDispProfile { get; set; }
        public int StateID { get; set; }
        public string Background_Image { get; set; }
        public int Background_Color { get; set; }
        public int Background_Type { get; set; }
        public int SlideShow_Timer { get; set; }
        public byte Ignore_Background { get; set; }
        public byte Journal_Visible { get; set; }
        public int Journal_XPos { get; set; }
        public int Journal_YPos { get; set; }
        public int Journal_Width { get; set; }
        public int Journal_Height { get; set; }
        public int Journal_Borders { get; set; }
        public byte Bitmap_Visible { get; set; }
        public int Bitmap_XPos { get; set; }
        public int Bitmap_YPos { get; set; }
        public int Bitmap_Width { get; set; }
        public int Bitmap_Height { get; set; }
        public string Bitmap_Image { get; set; }
        public int Bitmap_BgColor { get; set; }
        public int Bitmap_Borders { get; set; }
        public byte Totals_Visible { get; set; }
        public int Totals_XPos { get; set; }
        public int Totals_YPos { get; set; }
        public int Totals_Width { get; set; }
        public int Totals_Height { get; set; }
        public int Totals_Borders { get; set; }
        public string Totals_Total_Font { get; set; }
        public int Totals_Total_FgColor { get; set; }
        public int Totals_Total_BgColor { get; set; }
        public string Totals_Discount_Font { get; set; }
        public int Totals_Discount_FgColor { get; set; }
        public int Totals_Discount_BgColor { get; set; }
        public string Totals_BalanceAmount_Font { get; set; }
        public int Totals_BalanceAmount_FgColor { get; set; }
        public int Totals_BalanceAmount_BgColor { get; set; }
        public string Totals_Balance_Font { get; set; }
        public int Totals_Balance_FgColor { get; set; }
        public int Totals_Balance_BgColor { get; set; }
        public string Totals_Payment_Font { get; set; }
        public int Totals_Payment_FgColor { get; set; }
        public int Totals_Payment_BgColor { get; set; }
        public int Totals_Rows { get; set; }
        public int Totals_Amount_Row { get; set; }
        public int Totals_Discount_Row { get; set; }
        public int Totals_Payment_Row { get; set; }
        public int Totals_Balance_Row { get; set; }
        public int Totals_Amount_Row_Height { get; set; }
        public int Totals_Discount_Row_Height { get; set; }
        public int Totals_Payment_Row_Height { get; set; }
        public int Totals_Balance_Row_Height { get; set; }
        public string Totals_Amount_Text { get; set; }
        public string Totals_Discount_Text { get; set; }
        public string Totals_Payment_Text { get; set; }
        public string Totals_Balance_Text { get; set; }
        public byte Browser_Visible { get; set; }
        public int Browser_XPos { get; set; }
        public int Browser_YPos { get; set; }
        public int Browser_Width { get; set; }
        public int Browser_Height { get; set; }
        public string Browser_URL { get; set; }
        public int Browser_Borders { get; set; }
        public byte ItemList_Visible { get; set; }
        public int ItemList_XPos { get; set; }
        public int ItemList_YPos { get; set; }
        public int ItemList_Width { get; set; }
        public int ItemList_Height { get; set; }
        public int ItemList_Borders { get; set; }
    }
}
