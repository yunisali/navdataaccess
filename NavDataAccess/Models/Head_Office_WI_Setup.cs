//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace NavDataAccess.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Head_Office_WI_Setup
    {
        public byte[] timestamp { get; set; }
        public string Code { get; set; }
        public byte WI_in_Use { get; set; }
        public byte Web_POS_Resource_Locking { get; set; }
        public string Web_Store_Code { get; set; }
        public string Web_Store_Customer_No_ { get; set; }
        public string Web_Store_Club_Code { get; set; }
        public int Web_Store_Create_Sales_Order { get; set; }
        public string Web_Store_POS_Terminal { get; set; }
        public string Web_Store_Staff_ID { get; set; }
        public string Web_Store_Display_Locale { get; set; }
        public int Web_Store_Last_Action_Entry_No { get; set; }
    }
}
