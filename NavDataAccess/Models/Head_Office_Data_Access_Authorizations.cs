//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace NavDataAccess.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Head_Office_Data_Access_Authorizations
    {
        public byte[] timestamp { get; set; }
        public string Store_No_ { get; set; }
        public string Retail_User_ID { get; set; }
        public int Table_ID { get; set; }
    }
}
