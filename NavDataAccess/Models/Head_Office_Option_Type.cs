//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace NavDataAccess.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Head_Office_Option_Type
    {
        public byte[] timestamp { get; set; }
        public string Option_Type { get; set; }
        public string Description { get; set; }
        public byte Mandatory { get; set; }
        public byte Fixed_Input { get; set; }
        public byte Use_In_Labels { get; set; }
    }
}
