//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace NavDataAccess.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Head_Office_xWH___Country_State_DISCONT_
    {
        public byte[] timestamp { get; set; }
        public int No_ { get; set; }
        public string Country_Code { get; set; }
        public string State_Name { get; set; }
        public string State_Short_Name { get; set; }
    }
}
