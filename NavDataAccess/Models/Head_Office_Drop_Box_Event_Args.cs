//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace NavDataAccess.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Head_Office_Drop_Box_Event_Args
    {
        public byte[] timestamp { get; set; }
        public int ID { get; set; }
        public int Table_ID { get; set; }
        public string Position { get; set; }
        public string Drop_Box_Code { get; set; }
        public int Page_Drop_Box_Index { get; set; }
    }
}
