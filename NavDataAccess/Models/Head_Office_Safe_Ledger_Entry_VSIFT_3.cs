//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace NavDataAccess.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Head_Office_Safe_Ledger_Entry_VSIFT_3
    {
        public string Safe_No_ { get; set; }
        public string Tender_Type { get; set; }
        public string Currency_Code { get; set; }
        public byte Open { get; set; }
        public byte Positive { get; set; }
        public System.DateTime Posting_Date { get; set; }
        public Nullable<long> C_Cnt { get; set; }
        public Nullable<decimal> SUM_Amount__LCY_ { get; set; }
        public Nullable<decimal> SUM_Amount { get; set; }
        public Nullable<decimal> SUM_Remaining_Amount__LCY_ { get; set; }
        public Nullable<decimal> SUM_Remaining_Amount { get; set; }
    }
}
