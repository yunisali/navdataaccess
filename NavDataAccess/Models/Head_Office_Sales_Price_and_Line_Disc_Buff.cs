//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace NavDataAccess.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Head_Office_Sales_Price_and_Line_Disc_Buff
    {
        public byte[] timestamp { get; set; }
        public int Line_Type { get; set; }
        public int Type { get; set; }
        public string Code { get; set; }
        public int Sales_Type { get; set; }
        public string Sales_Code { get; set; }
        public System.DateTime Starting_Date { get; set; }
        public string Currency_Code { get; set; }
        public string Variant_Code { get; set; }
        public string Unit_of_Measure_Code { get; set; }
        public decimal Minimum_Quantity { get; set; }
        public string Loaded_Item_No_ { get; set; }
        public string Loaded_Disc__Group { get; set; }
        public string Loaded_Customer_No_ { get; set; }
        public string Loaded_Price_Group { get; set; }
        public decimal Line_Discount__ { get; set; }
        public decimal Unit_Price { get; set; }
        public byte Price_Includes_VAT { get; set; }
        public byte Allow_Invoice_Disc_ { get; set; }
        public string VAT_Bus__Posting_Gr___Price_ { get; set; }
        public System.DateTime Ending_Date { get; set; }
        public byte Allow_Line_Disc_ { get; set; }
    }
}
