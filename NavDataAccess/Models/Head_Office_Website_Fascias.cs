//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace NavDataAccess.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Head_Office_Website_Fascias
    {
        public byte[] timestamp { get; set; }
        public string No_ { get; set; }
        public string Country_Code { get; set; }
        public string Description { get; set; }
        public string Store_No_ { get; set; }
        public string POS_Terminal_No_ { get; set; }
        public string POS_Slip_No_ { get; set; }
        public string Staff_ID { get; set; }
        public string Customer_No_ { get; set; }
        public string Stock_Document_No__Series { get; set; }
        public string POS_Slip_No__Series { get; set; }
        public byte Blocked { get; set; }
    }
}
