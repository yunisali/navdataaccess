//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace NavDataAccess.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Head_Office_KDS_Display_Station
    {
        public byte[] timestamp { get; set; }
        public string Restaurant_No_ { get; set; }
        public string ID { get; set; }
        public string Description { get; set; }
        public string Functional_Profile { get; set; }
        public string Style_Profile { get; set; }
        public string Aging_Style_Profile { get; set; }
        public string Visual_Profile { get; set; }
        public int Screen_Location { get; set; }
        public int Screen_Number { get; set; }
        public byte Station_is_Backup_Printer { get; set; }
        public int Compress_BOM_Receipt { get; set; }
        public string Exclude_from_Compression { get; set; }
        public int Display_Station_Type { get; set; }
        public string Printer_ID { get; set; }
        public string Backup_Printer_Station_ID { get; set; }
        public byte Use_Backup_Printer_Station { get; set; }
    }
}
