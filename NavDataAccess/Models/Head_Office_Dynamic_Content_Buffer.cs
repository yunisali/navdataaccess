//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace NavDataAccess.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Head_Office_Dynamic_Content_Buffer
    {
        public byte[] timestamp { get; set; }
        public int EntryNo { get; set; }
        public string SectionId { get; set; }
        public int ParentId { get; set; }
        public int Level { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }
        public string TimeWithinBounds { get; set; }
        public string EndTimeAfterMidnight { get; set; }
        public string EntryPriceGroup { get; set; }
        public string EntryUoM { get; set; }
        public string EntryType { get; set; }
        public string EntryId { get; set; }
        public string Description { get; set; }
        public int Order { get; set; }
        public string ItemId { get; set; }
        public string ItemType { get; set; }
        public string ItemDescription { get; set; }
        public decimal ItemPrice { get; set; }
        public string ItemUoM { get; set; }
        public string ItemPriceGr { get; set; }
        public int ItemDefaultMenuType { get; set; }
        public string IngredientItemId { get; set; }
        public decimal BOMQuantity { get; set; }
        public string BOMUoM { get; set; }
        public decimal MinQty { get; set; }
        public string ItemModGroupId { get; set; }
        public string ItemModGroupDescription { get; set; }
        public string ItemModPriceType { get; set; }
        public string TextModifier { get; set; }
        public decimal ItemModPricePercentage { get; set; }
        public decimal ItemModQty { get; set; }
        public string ItemModUoM { get; set; }
        public decimal MinSelection { get; set; }
        public decimal MaxSelection { get; set; }
        public string ItemModRequired { get; set; }
        public string DealModGroupId { get; set; }
        public string DealModGroupDescription { get; set; }
        public decimal DealModPrice { get; set; }
        public string DealModRequired { get; set; }
        public string OfferId { get; set; }
        public string OfferDescription { get; set; }
        public string OfferLineType { get; set; }
        public int OfferLineId { get; set; }
        public string DefaultItemId { get; set; }
        public string OfferLineDescription { get; set; }
        public decimal OfferPrice { get; set; }
        public decimal OfferLineQty { get; set; }
        public string OfferSecondaryDescription { get; set; }
        public string ImageId { get; set; }
    }
}
