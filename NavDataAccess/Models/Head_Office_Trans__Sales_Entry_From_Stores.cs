//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace NavDataAccess.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Head_Office_Trans__Sales_Entry_From_Stores
    {
        public byte[] timestamp { get; set; }
        public string Store_No_ { get; set; }
        public string POS_Terminal_No_ { get; set; }
        public int Transaction_No_ { get; set; }
        public int Line_No_ { get; set; }
        public string Receipt_No_ { get; set; }
        public string Barcode_No_ { get; set; }
        public string Item_No_ { get; set; }
        public string Sales_Staff { get; set; }
        public string Item_Category_Code { get; set; }
        public string Product_Group_Code { get; set; }
        public decimal Price { get; set; }
        public decimal Net_Price { get; set; }
        public decimal Quantity { get; set; }
        public string VAT_Code { get; set; }
        public int Transaction_Status { get; set; }
        public decimal Discount_Amount { get; set; }
        public decimal Cost_Amount { get; set; }
        public System.DateTime Date { get; set; }
        public System.DateTime Time { get; set; }
        public string Shift_No_ { get; set; }
        public System.DateTime Shift_Date { get; set; }
        public decimal Net_Amount { get; set; }
        public decimal VAT_Amount { get; set; }
        public string Promotion_No_ { get; set; }
        public decimal Standard_Net_Price { get; set; }
        public decimal Disc__Amount_From_Std__Price { get; set; }
        public string Statement_No_ { get; set; }
        public string Customer_No_ { get; set; }
        public string Section { get; set; }
        public string Shelf { get; set; }
        public string Statement_Code { get; set; }
        public string Item_Disc__Group { get; set; }
        public int Transaction_Code { get; set; }
        public byte Item_Number_Scanned { get; set; }
        public byte Keyboard_Item_Entry { get; set; }
        public byte Price_in_Barcode { get; set; }
        public byte Price_Change { get; set; }
        public byte Weight_Manually_Entered { get; set; }
        public byte Line_was_Discounted { get; set; }
        public byte Scale_Item { get; set; }
        public byte Weight_Item { get; set; }
        public byte Return_No_Sale { get; set; }
        public byte Item_Corrected_Line { get; set; }
        public int Type_of_Sale { get; set; }
        public byte Linked_No__not_Orig_ { get; set; }
        public byte Orig__of_a_Linked_Item_List { get; set; }
        public string Staff_ID { get; set; }
        public string Item_Posting_Group { get; set; }
        public decimal Total_Rounded_Amt_ { get; set; }
        public decimal Counter { get; set; }
        public string Variant_Code { get; set; }
        public string Serial_No_ { get; set; }
        public byte Serial_No__Not_Valid { get; set; }
        public decimal Line_Discount { get; set; }
        public byte Replicated { get; set; }
        public decimal Customer_Discount { get; set; }
        public decimal Infocode_Discount { get; set; }
        public decimal Cust__Invoice_Discount { get; set; }
        public string Unit_of_Measure { get; set; }
        public decimal UOM_Quantity { get; set; }
        public decimal UOM_Price { get; set; }
        public decimal Total_Discount { get; set; }
        public decimal Total_Disc__ { get; set; }
        public int Tot__Disc_Info_Line_No_ { get; set; }
        public int Periodic_Disc__Type { get; set; }
        public string Periodic_Disc__Group { get; set; }
        public decimal Periodic_Discount { get; set; }
        public byte Deal_Line { get; set; }
        public decimal Discount_Amt__For_Printing { get; set; }
        public string Approval_ID { get; set; }
        public string Season_Code { get; set; }
        public string Fascia { get; set; }
        public decimal Gross_Profit { get; set; }
        public string NORMALTRANLOG_BARCODE_ID { get; set; }
        public string Store_Group_Code { get; set; }
        public string Product_Sub_Group_Code { get; set; }
        public string Orig_Trans_Store { get; set; }
        public string Orig_Trans_Pos { get; set; }
        public int Orig_Trans_No_ { get; set; }
        public int Orig_Trans_Line_No_ { get; set; }
        public decimal Max_Qty_to_Refund { get; set; }
        public decimal Refund_Qty_ { get; set; }
        public int Refunded_Line_No_ { get; set; }
        public int Refunded_Trans__No_ { get; set; }
        public string Refunded_POS_No_ { get; set; }
        public string Refunded_Store_No_ { get; set; }
        public byte Line_Selected { get; set; }
        public string Item_Division_Code { get; set; }
        public string Sales_Order_No_ { get; set; }
        public int Sales_Order_Line { get; set; }
        public string Item_Brand_Code { get; set; }
        public int Cluster_Entry_No_ { get; set; }
    }
}
