//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace NavDataAccess.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Head_Office_Label_Functions
    {
        public byte[] timestamp { get; set; }
        public int Type { get; set; }
        public string Label_Code { get; set; }
        public int Run_Codeunit { get; set; }
        public byte Print_to_File { get; set; }
    }
}
