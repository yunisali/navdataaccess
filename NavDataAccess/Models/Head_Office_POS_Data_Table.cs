//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace NavDataAccess.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Head_Office_POS_Data_Table
    {
        public byte[] timestamp { get; set; }
        public string Data_Table_ID { get; set; }
        public string Description { get; set; }
        public int Table_No_ { get; set; }
        public int Table_Columns { get; set; }
        public int Table_Rows { get; set; }
        public int Key_Value_Field_1 { get; set; }
        public int Key_Value_Field_2 { get; set; }
        public int Key_Value_Field_3 { get; set; }
        public string Start_Position { get; set; }
        public int Selection_Mode { get; set; }
        public byte Temp__Data { get; set; }
        public byte Insert_Allowed { get; set; }
        public byte Delete_Allowed { get; set; }
        public int Default_Search_Type { get; set; }
        public string Default_Search_Command { get; set; }
        public int Minimum_Width { get; set; }
        public string Distribution_Location { get; set; }
        public string POS_Module { get; set; }
    }
}
