//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace NavDataAccess.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Head_Office_Offline_Call_Center_Setup
    {
        public byte[] timestamp { get; set; }
        public string Key { get; set; }
        public string Get_Estimated_Timing_Job_ID { get; set; }
        public string Get_Status_on_Orders_Job_ID { get; set; }
        public string Process_Call_Cent__Ord__Job_ID { get; set; }
        public byte Send_Status_Chg__to_Call_Cent_ { get; set; }
        public string Send_Conv__Pre_Orders_Job_ID { get; set; }
        public string Delete_Web_Service_Log_Job_ID { get; set; }
        public int Days_Web_Serv__Log_Entr__Exist { get; set; }
        public string Scheduler_Job_Type { get; set; }
        public byte Show_Msg__on_Successful_Send { get; set; }
    }
}
