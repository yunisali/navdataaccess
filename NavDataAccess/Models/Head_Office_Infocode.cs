//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace NavDataAccess.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Head_Office_Infocode
    {
        public byte[] timestamp { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public string Prompt { get; set; }
        public int Display_Option { get; set; }
        public byte Once_per_Transaction { get; set; }
        public byte Value_is_Amt__Qty_ { get; set; }
        public int Print_Item_Modifier_on_Receipt { get; set; }
        public byte Print_Prompt_on_Receipt { get; set; }
        public byte Print_Input_on_Receipt { get; set; }
        public byte Print_Inp__Name_on_Rcpt_ { get; set; }
        public int Type { get; set; }
        public int Usage_Category { get; set; }
        public int Usage_Sub_Category { get; set; }
        public decimal Min__Value { get; set; }
        public decimal Max__Value { get; set; }
        public int Min__Length { get; set; }
        public int Max__Length { get; set; }
        public byte Input_Required { get; set; }
        public byte Std_1__in_Value { get; set; }
        public string Linked_Infocode { get; set; }
        public decimal Random_Factor__ { get; set; }
        public decimal Random_Counter { get; set; }
        public string Infocode_Validation_Period_ID { get; set; }
        public string Data_Entry_Type { get; set; }
        public byte Link_Item_Lines_to_Trigg__Line { get; set; }
        public byte Multiple_Selection { get; set; }
        public int Triggering { get; set; }
        public int Min__Selection { get; set; }
        public int Max__Selection { get; set; }
        public string Explanatory_Header_Text { get; set; }
        public int OK_Pressed_Action { get; set; }
        public int Selection_Type { get; set; }
        public int Quantity_Handling { get; set; }
        public byte Mobile___Display_Required { get; set; }
    }
}
