//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace NavDataAccess.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class pt_reindex_profiles
    {
        public int Profile_ID { get; set; }
        public int Include_Exclude { get; set; }
        public string Company_Name { get; set; }
        public int Table_ID { get; set; }
    }
}
