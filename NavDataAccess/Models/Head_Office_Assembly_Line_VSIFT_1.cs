//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace NavDataAccess.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Head_Office_Assembly_Line_VSIFT_1
    {
        public int Document_Type { get; set; }
        public string Document_No_ { get; set; }
        public int Type { get; set; }
        public string Location_Code { get; set; }
        public Nullable<long> C_Cnt { get; set; }
        public Nullable<decimal> SUM_Cost_Amount { get; set; }
        public Nullable<decimal> SUM_Quantity { get; set; }
    }
}
