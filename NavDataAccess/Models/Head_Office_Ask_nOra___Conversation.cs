//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace NavDataAccess.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Head_Office_Ask_nOra___Conversation
    {
        public byte[] timestamp { get; set; }
        public int ID { get; set; }
        public string User_ID { get; set; }
        public System.DateTime Started_DateTime { get; set; }
        public System.DateTime Last_Activity_DateTime { get; set; }
        public byte Ended { get; set; }
        public byte Last_Function_Success { get; set; }
        public int Need_Answer_to_Question { get; set; }
        public string Question_Text_Answer { get; set; }
        public int Question_Answered { get; set; }
        public int Question_Choice_Answer { get; set; }
        public byte Said_Hello { get; set; }
        public System.DateTime Said_Hello_DateTime { get; set; }
        public byte Last_Action_Success { get; set; }
        public int Process_Sentence_Fail_Counter { get; set; }
        public byte System_Use { get; set; }
        public byte Debug_Mode { get; set; }
        public int Last_Sentence_ID_Seen { get; set; }
        public string Curr__Function { get; set; }
        public string Curr__Action { get; set; }
        public int Curr__Table { get; set; }
        public int Curr__Field { get; set; }
        public string Curr__Rec__Position { get; set; }
        public int Curr__Sentence_ID { get; set; }
        public int Curr__Field_Type { get; set; }
        public string Prev__Function { get; set; }
        public string Prev__Action { get; set; }
        public int Prev__Table { get; set; }
        public int Prev__Field { get; set; }
        public string Prev__Rec__Position { get; set; }
        public int Prev__Sentence_ID { get; set; }
        public int Prev__Field_Type { get; set; }
        public string Before_Question_Function { get; set; }
        public string Before_Question_Action { get; set; }
        public int Before_Question_Table { get; set; }
        public int Before_Question_Field { get; set; }
        public string Before_Question_Rec__Position { get; set; }
        public int Before_Question_Sentence_ID { get; set; }
        public int Before_Question_Field_Type { get; set; }
        public string Before_Q__Prev__Function { get; set; }
        public string Before_Q__Prev__Action { get; set; }
        public int Before_Q__Prev__Table { get; set; }
        public int Before_Q__Prev__Field { get; set; }
        public string Before_Q__Prev__Rec__Position { get; set; }
        public int Before_Q__Prev__Sentence_ID { get; set; }
        public int Before_Q__Prev__Field_Type { get; set; }
    }
}
