//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace NavDataAccess.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Head_Office_Enhanced_Chart_Type
    {
        public byte[] timestamp { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public byte[] Picture { get; set; }
        public byte Blocked { get; set; }
        public int Sequence { get; set; }
        public int View_Type { get; set; }
        public byte Default { get; set; }
        public int Min__No__of_Series { get; set; }
        public string Created_By { get; set; }
        public System.DateTime Created_Date_Time { get; set; }
        public string Modified_By { get; set; }
        public System.DateTime Modified_Date_Time { get; set; }
    }
}
