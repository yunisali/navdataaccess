//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace NavDataAccess.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Head_Office_Trans__Address_From_Stores
    {
        public byte[] timestamp { get; set; }
        public string Store_No_ { get; set; }
        public string POS_No_ { get; set; }
        public int Transaction_No_ { get; set; }
        public string Receipt_No_ { get; set; }
        public string First_Name { get; set; }
        public string Middle_Name { get; set; }
        public string Last_Name { get; set; }
        public string Full_Name { get; set; }
        public string Address_1 { get; set; }
        public string Address_2 { get; set; }
        public string City { get; set; }
        public string County { get; set; }
        public string Postcode { get; set; }
        public string Telephone { get; set; }
        public string Email { get; set; }
        public string Country_Code { get; set; }
        public string House_No_ { get; set; }
        public decimal Order_Deposit_Amount { get; set; }
        public string Existing_Customer { get; set; }
        public string Existing_Contact { get; set; }
        public string Customer_Order_No_ { get; set; }
        public string Order_No_ { get; set; }
        public string Ship_to_Address_1 { get; set; }
        public string Ship_to_Address_2 { get; set; }
        public string Ship_to_City { get; set; }
        public string Ship_to_County { get; set; }
        public string Ship_to_Postcode { get; set; }
        public string Ship_to_Telephone { get; set; }
        public string Ship_to_Country_Code { get; set; }
        public string Ship_to_House_No_ { get; set; }
        public string General_Comments { get; set; }
        public string Delivery_Instructions { get; set; }
        public int Document_Type { get; set; }
        public string Fulfilment_Location { get; set; }
        public byte Financed { get; set; }
        public string Finance_Agreement_No_ { get; set; }
        public decimal Financed_Amount { get; set; }
        public int Shipping_Advice { get; set; }
        public string Title { get; set; }
        public int Contactable { get; set; }
        public byte Cont__Mail { get; set; }
        public byte Cont__Telephone { get; set; }
        public byte Cont__E_mail { get; set; }
        public string Daytime_Phone_No_ { get; set; }
        public string Source_Code { get; set; }
        public string Mobile_Phone_No_ { get; set; }
    }
}
