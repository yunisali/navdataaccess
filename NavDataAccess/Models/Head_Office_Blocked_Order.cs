//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace NavDataAccess.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Head_Office_Blocked_Order
    {
        public byte[] timestamp { get; set; }
        public int No_ { get; set; }
        public string Order_No_ { get; set; }
        public int Reason { get; set; }
        public byte Blocked { get; set; }
        public System.DateTime OrderDate { get; set; }
    }
}
