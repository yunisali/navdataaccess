//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace NavDataAccess.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Head_Office_Allocation_Plan_Group_Lines
    {
        public byte[] timestamp { get; set; }
        public string Allocation_Plan_Code { get; set; }
        public string Item_No_ { get; set; }
        public string Variant_Dimension_1_Code { get; set; }
        public string Group_Code { get; set; }
        public int Destination_Type { get; set; }
        public string Destination_Code { get; set; }
        public string Store_No_ { get; set; }
        public decimal Group___Share { get; set; }
        public decimal Destination_Weight { get; set; }
        public decimal C__Share { get; set; }
        public decimal Quantity { get; set; }
        public decimal Qty__from_Lines { get; set; }
        public decimal Minimum_Quantity { get; set; }
        public decimal Maximum_Quantity { get; set; }
        public byte Location_is_a_Buffer { get; set; }
        public decimal Buffer_Precent_of_Total { get; set; }
        public string Warehouse_Buffer_Location { get; set; }
        public decimal Buffer__ { get; set; }
        public string Remarks { get; set; }
        public int Group_Sort_Order { get; set; }
    }
}
