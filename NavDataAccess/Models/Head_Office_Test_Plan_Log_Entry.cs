//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace NavDataAccess.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Head_Office_Test_Plan_Log_Entry
    {
        public byte[] timestamp { get; set; }
        public int Entry_No_ { get; set; }
        public string Test_Plan_No_ { get; set; }
        public System.DateTime Test_Start { get; set; }
        public System.DateTime Test_End { get; set; }
        public decimal Test_Duration { get; set; }
        public int No__of_Sessions { get; set; }
        public int No__of_Session_Tasks { get; set; }
        public int Total_No__of_Tasks { get; set; }
    }
}
