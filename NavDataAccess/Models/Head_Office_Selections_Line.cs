//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace NavDataAccess.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Head_Office_Selections_Line
    {
        public byte[] timestamp { get; set; }
        public int Table_No_ { get; set; }
        public string Selections_Code { get; set; }
        public int Line_No_ { get; set; }
        public string Position_Text { get; set; }
        public string Primary_Key_1_Value { get; set; }
        public string Primary_Key_2_Value { get; set; }
        public string Primary_Key_3_Value { get; set; }
        public string Primary_Key_4_Value { get; set; }
        public string Primary_Key_5_Value { get; set; }
        public string Record_Name { get; set; }
        public int Record_Name_Field_No_ { get; set; }
    }
}
