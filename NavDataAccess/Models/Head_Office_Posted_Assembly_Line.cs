//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace NavDataAccess.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Head_Office_Posted_Assembly_Line
    {
        public byte[] timestamp { get; set; }
        public string Document_No_ { get; set; }
        public int Line_No_ { get; set; }
        public string Order_No_ { get; set; }
        public int Order_Line_No_ { get; set; }
        public int Type { get; set; }
        public string No_ { get; set; }
        public string Variant_Code { get; set; }
        public string Description { get; set; }
        public string Description_2 { get; set; }
        public string Lead_Time_Offset { get; set; }
        public int Resource_Usage_Type { get; set; }
        public string Location_Code { get; set; }
        public string Shortcut_Dimension_1_Code { get; set; }
        public string Shortcut_Dimension_2_Code { get; set; }
        public string Bin_Code { get; set; }
        public string Position { get; set; }
        public string Position_2 { get; set; }
        public string Position_3 { get; set; }
        public int Item_Shpt__Entry_No_ { get; set; }
        public decimal Quantity { get; set; }
        public decimal Quantity__Base_ { get; set; }
        public System.DateTime Due_Date { get; set; }
        public decimal Quantity_per { get; set; }
        public decimal Qty__per_Unit_of_Measure { get; set; }
        public string Inventory_Posting_Group { get; set; }
        public string Gen__Prod__Posting_Group { get; set; }
        public decimal Unit_Cost { get; set; }
        public decimal Cost_Amount { get; set; }
        public string Unit_of_Measure_Code { get; set; }
        public int Dimension_Set_ID { get; set; }
    }
}
