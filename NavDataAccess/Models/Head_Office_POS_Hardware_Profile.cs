//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace NavDataAccess.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Head_Office_POS_Hardware_Profile
    {
        public byte[] timestamp { get; set; }
        public string Profile_ID { get; set; }
        public string Description { get; set; }
        public int Drawer_Alert_Timeout { get; set; }
        public byte Drawer_Alert_if_Open { get; set; }
        public int Drawer_Status { get; set; }
        public int Drawer { get; set; }
        public string Drawer_Device_Name { get; set; }
        public string Drawer_Open_Text { get; set; }
        public string Drawer_Description { get; set; }
        public byte Display_Bin__Conversion { get; set; }
        public int Display { get; set; }
        public string Display_Device_Name { get; set; }
        public string Display_Description { get; set; }
        public string Display_Total_Text { get; set; }
        public string Display_Balance_Text { get; set; }
        public string Display_Closed_Line1 { get; set; }
        public string Display_Closed_Line2 { get; set; }
        public int Display_Characterset { get; set; }
        public int Delay_for_Linked_items { get; set; }
        public byte Use_Non_Payment_Track_Handling { get; set; }
        public int Start_from_Position { get; set; }
        public int End_at_Position { get; set; }
        public string End_at_Char { get; set; }
        public int MSR { get; set; }
        public string MSR_Device_Name { get; set; }
        public string MSR_Description { get; set; }
        public byte MSR_Autodisable { get; set; }
        public byte MSR_Skip_Close_On_ClearInput { get; set; }
        public byte MSR_Read_Track_1 { get; set; }
        public byte MSR_Read_Track_2 { get; set; }
        public byte MSR_Read_Track_3 { get; set; }
        public byte Fiscal_Printer { get; set; }
        public byte Printer_Remote { get; set; }
        public int No__of_Lines_on_Invoice { get; set; }
        public byte Print_Discount_Detail { get; set; }
        public string Printer_Normal { get; set; }
        public int Printer { get; set; }
        public string Printer_Device_Name { get; set; }
        public string Printer_Bold { get; set; }
        public string Printer_Wide { get; set; }
        public string Printer_High { get; set; }
        public string Printer_Wide___High { get; set; }
        public int Printer_Characterset { get; set; }
        public string Printer_Description { get; set; }
        public string Print_Server_Host { get; set; }
        public int Print_Server_Port { get; set; }
        public string Printer_Italic { get; set; }
        public int Decimals_in_Entry { get; set; }
        public byte Printer_Sharing { get; set; }
        public int Logo_Size { get; set; }
        public string Logo { get; set; }
        public int Logo_Type { get; set; }
        public int Logo_Align { get; set; }
        public int Logo_No_ { get; set; }
        public int Max__Invoice_Lines { get; set; }
        public byte Print_Bin__Conversion { get; set; }
        public int Hard_Totals { get; set; }
        public string Hard_Totals_Device_Name { get; set; }
        public string Hard_Totals_Description { get; set; }
        public byte Hard_Totals_Remote { get; set; }
        public int Keyboard { get; set; }
        public string Keyboard_Device_Name { get; set; }
        public string Keyboard_Description { get; set; }
        public byte Keyboard_Remote { get; set; }
        public byte Find_As_You_Type { get; set; }
        public byte ENTER_Repeats_Last_Item { get; set; }
        public byte Fast_Input_as_Wedge { get; set; }
        public int Fast_Input_Character_Interval { get; set; }
        public int Fast_Input_Type { get; set; }
        public byte Tone_Remote { get; set; }
        public int Tone_Indicator { get; set; }
        public string Tone_Device_Name { get; set; }
        public string Tone_Description { get; set; }
        public int Tone1_Pitch { get; set; }
        public int Tone1_Volume { get; set; }
        public int Tone1_Duration { get; set; }
        public int Tone2_Pitch { get; set; }
        public int Tone2_Volume { get; set; }
        public int Tone2_Duration { get; set; }
        public int Inter_Tone_Wait { get; set; }
        public int Scanner { get; set; }
        public string Scanner_Device_Name { get; set; }
        public string Scanner_Description { get; set; }
        public byte Scanner_Autodisable { get; set; }
        public int Scanner_DataLabel_Start_Index { get; set; }
        public int Scale { get; set; }
        public string Scale_Device_Name { get; set; }
        public int Timeout_in_Sec_ { get; set; }
        public string Scale_Description { get; set; }
        public byte Manual_Input_Allowed { get; set; }
        public int Currency_Symbol_as_ASCII { get; set; }
        public int Keylock { get; set; }
        public string Keylock_Device_Name { get; set; }
        public string Keylock_Description { get; set; }
        public int No__of_LF_at_Receipt_End { get; set; }
        public int No__of_LF_at_Invoice_Top { get; set; }
        public int Doc__Insert_Removal_Timeout { get; set; }
        public int Left_Margin_on_Receipt { get; set; }
        public int EFT { get; set; }
        public string EFT_Server_Name { get; set; }
        public int EFT_Server_Port { get; set; }
        public string EFT_Description { get; set; }
        public string EFT_Company_ID { get; set; }
        public string EFT_User_ID { get; set; }
        public string EFT_Password { get; set; }
        public byte Capture_Extra_Data { get; set; }
        public int MICR { get; set; }
        public string MICR_Driver_Name { get; set; }
        public string MICR_Description { get; set; }
        public byte Show_Journal_at_Logoff { get; set; }
        public int Dallas_Key { get; set; }
        public string Dallas_Key_Description { get; set; }
        public string Dallas_Key_Port { get; set; }
        public int Dallas_Key_BaudRate { get; set; }
        public int Dallas_Key_Parity { get; set; }
        public int Dallas_Key_DataBits { get; set; }
        public int Dallas_Key_FlowControl { get; set; }
        public int Dallas_Key_StopBits { get; set; }
        public string Dallas_Key_Logon_Prefix { get; set; }
        public string Dallas_Key_Logoff_Prefix { get; set; }
        public byte Dallas_Key_ClosePosOnLogoff { get; set; }
        public int FC_Pos_Port { get; set; }
        public int FC_Port { get; set; }
        public string FC_Controller_Host_Name { get; set; }
        public int FC_Log_Level { get; set; }
        public string FC_Impl_File_Name { get; set; }
        public string FC_Impl_File_Type { get; set; }
        public int FC_Calling_Sound { get; set; }
        public int FC_Calling_Blink { get; set; }
        public int FC_Screen_Height__ { get; set; }
        public int FC_Screen_Ext_Height__ { get; set; }
        public int FC_Fuelling_Points_Columns { get; set; }
        public int FC_Resolution { get; set; }
        public string FC_Currency_Symbol { get; set; }
        public string FC_Culture_Name { get; set; }
        public string FC_Base_Unit_of_Measure_Code { get; set; }
        public byte Forecourt_in_use { get; set; }
        public string FC_Manager_Folder { get; set; }
        public byte FC_Mainstate_as_Text { get; set; }
        public byte FC_Sound_on_Driver_Off { get; set; }
        public string Printer_Impl__DLL { get; set; }
        public string Display_Impl__DLL { get; set; }
        public string MSR_Impl__DLL { get; set; }
        public string Drawer_Impl__DLL { get; set; }
        public string Scanner_Impl__DLL { get; set; }
        public string Scale_Impl__DLL { get; set; }
        public string KeyLock_Impl__DLL { get; set; }
        public string Tone_Impl__DLL { get; set; }
        public string MICR_Impl__DLL { get; set; }
        public string Printer_Impl__Type_Name { get; set; }
        public string Display_Impl__Type_Name { get; set; }
        public string MSR_Impl__Type_Name { get; set; }
        public string Drawer_Impl__Type_Name { get; set; }
        public string Scanner_Impl__Type_Name { get; set; }
        public string Scale_Impl__Type_Name { get; set; }
        public string KeyLock_Impl__Type_Name { get; set; }
        public string Tone_Impl__Type_Name { get; set; }
        public string MICR_Impl__Type_Name { get; set; }
        public int Scanner_ScanData_Start_Index { get; set; }
        public int Drawer_Beep_Timeout { get; set; }
        public int Drawer_Beep_Delay { get; set; }
        public int Drawer_Beep_Frequency { get; set; }
        public int Drawer_Beep_Duration { get; set; }
        public string Dallas_Key_Impl__DLL { get; set; }
        public string Dallas_Key_Impl__Type_Name { get; set; }
        public int POS_Smoothing { get; set; }
        public int POS_TextRendering { get; set; }
        public byte OPOS_Windows_Printing { get; set; }
        public string Windows_Printer { get; set; }
        public int Printer_Cut_Percentage { get; set; }
        public int EFT_Provider { get; set; }
    }
}
