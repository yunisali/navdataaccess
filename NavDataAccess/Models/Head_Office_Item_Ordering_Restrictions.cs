//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace NavDataAccess.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Head_Office_Item_Ordering_Restrictions
    {
        public byte[] timestamp { get; set; }
        public string Item_Category_Code { get; set; }
        public string Product_Group_Code { get; set; }
        public string Item_No_ { get; set; }
        public string Variant_Dimension_1_Code { get; set; }
        public string Variant_Code { get; set; }
        public string Store_Group_Code { get; set; }
        public string Location_Code { get; set; }
        public int Status { get; set; }
        public int Sourcing { get; set; }
        public int Vendor_Deliver_to { get; set; }
        public int Deliver_from { get; set; }
        public string Store_Delivery_Location_Code { get; set; }
        public int Delivery_Method { get; set; }
        public string Whse_Delivery_Location_Code { get; set; }
    }
}
