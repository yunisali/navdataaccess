//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace NavDataAccess.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Head_Office_Excel_Form_Range
    {
        public byte[] timestamp { get; set; }
        public string Workbook_Name { get; set; }
        public string Start_Address { get; set; }
        public int Found_at_Row { get; set; }
        public string Address { get; set; }
        public string NAV_Field_Name { get; set; }
        public byte Repeat_on_Field { get; set; }
        public byte Allow_Blank { get; set; }
        public int Column_Offset { get; set; }
        public string Set_to_Value_Text { get; set; }
        public byte Auto_Increment_Field { get; set; }
    }
}
