//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace NavDataAccess.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Head_Office_KOT_Report_Header
    {
        public byte[] timestamp { get; set; }
        public string KOT_No_ { get; set; }
        public string Receipt_No_ { get; set; }
        public decimal NetSales { get; set; }
        public decimal Guests { get; set; }
        public string Restaurant_No_ { get; set; }
        public decimal Checks { get; set; }
        public string FixedPeriod { get; set; }
        public decimal Production_Time { get; set; }
        public decimal Expected_Production_Time { get; set; }
        public decimal Recall_Count { get; set; }
        public decimal RushOrder_Count { get; set; }
        public decimal KDSOrders { get; set; }
        public System.DateTime Time_Started { get; set; }
        public System.DateTime Date_Filter { get; set; }
        public string Period { get; set; }
        public System.DateTime Sent_Time { get; set; }
        public System.DateTime Time_Bumped { get; set; }
        public string Display_Station_ID { get; set; }
        public decimal Minutes05 { get; set; }
        public decimal Minutes10 { get; set; }
        public decimal Minutes15 { get; set; }
        public decimal Minutes20 { get; set; }
        public decimal Minutes25 { get; set; }
        public decimal Minutes30 { get; set; }
        public decimal MinutesOver30 { get; set; }
        public decimal AlertOne { get; set; }
        public decimal AlertTwo { get; set; }
    }
}
