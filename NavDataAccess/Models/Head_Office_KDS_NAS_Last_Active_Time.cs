//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace NavDataAccess.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Head_Office_KDS_NAS_Last_Active_Time
    {
        public byte[] timestamp { get; set; }
        public string ID { get; set; }
        public System.DateTime Last_Active_Time { get; set; }
    }
}
