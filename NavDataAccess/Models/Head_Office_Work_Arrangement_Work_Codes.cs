//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace NavDataAccess.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Head_Office_Work_Arrangement_Work_Codes
    {
        public byte[] timestamp { get; set; }
        public string Shift_Code { get; set; }
        public string Work_Arrangement_Code { get; set; }
        public System.DateTime Working_Date { get; set; }
        public int Working_Day { get; set; }
        public System.DateTime Time_From { get; set; }
        public decimal Hours__qty_ { get; set; }
        public System.DateTime Time_To { get; set; }
        public string Work_Code { get; set; }
        public int Automatic_Acceptance { get; set; }
    }
}
