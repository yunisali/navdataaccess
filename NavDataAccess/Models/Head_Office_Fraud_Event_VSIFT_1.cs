//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace NavDataAccess.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Head_Office_Fraud_Event_VSIFT_1
    {
        public string Trigger_No_ { get; set; }
        public System.DateTime Trans__Date { get; set; }
        public System.DateTime Aggr__Time { get; set; }
        public Nullable<long> C_Cnt { get; set; }
        public Nullable<decimal> SUM_Trans__Value { get; set; }
        public Nullable<decimal> SUM_Record_Locator { get; set; }
    }
}
