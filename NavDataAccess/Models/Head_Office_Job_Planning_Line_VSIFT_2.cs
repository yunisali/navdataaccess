//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace NavDataAccess.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Head_Office_Job_Planning_Line_VSIFT_2
    {
        public string Job_No_ { get; set; }
        public string Job_Task_No_ { get; set; }
        public byte Contract_Line { get; set; }
        public System.DateTime Planning_Date { get; set; }
        public Nullable<long> C_Cnt { get; set; }
        public Nullable<decimal> SUM_Line_Amount__LCY_ { get; set; }
        public Nullable<decimal> SUM_Total_Price__LCY_ { get; set; }
        public Nullable<decimal> SUM_Total_Cost__LCY_ { get; set; }
    }
}
