//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace NavDataAccess.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Head_Office_Special_Day_Settings
    {
        public byte[] timestamp { get; set; }
        public System.DateTime Date { get; set; }
        public string Work_Location { get; set; }
        public int Day_Status { get; set; }
        public string Work_Code { get; set; }
        public string Description { get; set; }
        public string Date_Group { get; set; }
    }
}
