//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace NavDataAccess.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Head_Office_Trans__Infcd_Entry_From_Stores
    {
        public byte[] timestamp { get; set; }
        public string Store_No_ { get; set; }
        public string POS_Terminal_No_ { get; set; }
        public int Transaction_No_ { get; set; }
        public int Transaction_Type { get; set; }
        public int Line_No_ { get; set; }
        public string Infocode { get; set; }
        public string Information { get; set; }
        public decimal Info__Amt_ { get; set; }
        public System.DateTime Date { get; set; }
        public System.DateTime Time { get; set; }
        public string Staff_ID { get; set; }
        public string No_ { get; set; }
        public string Variant_Code { get; set; }
        public decimal Amount { get; set; }
        public int Type_of_Input { get; set; }
        public string Subcode { get; set; }
        public string Statement_No_ { get; set; }
        public string Statement_Code { get; set; }
        public string Source_Code { get; set; }
        public decimal Counter { get; set; }
        public byte Replicated { get; set; }
        public byte Transfer_Created { get; set; }
        public byte Updated { get; set; }
    }
}
