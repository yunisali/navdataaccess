//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace NavDataAccess.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Head_Office_Tile
    {
        public byte[] timestamp { get; set; }
        public string Panel_Code { get; set; }
        public int ID { get; set; }
        public int Tile_Group_ID { get; set; }
        public string Text { get; set; }
        public byte Show_Text { get; set; }
        public string Info { get; set; }
        public byte Show_Info { get; set; }
        public int Size { get; set; }
        public string Background_Colour_Code { get; set; }
        public byte[] Image { get; set; }
        public string Control_Name { get; set; }
        public byte[] Background_Colour_Bitmap { get; set; }
        public int Sequence { get; set; }
        public byte Blocked { get; set; }
        public int Target_Table_ID { get; set; }
        public string Target_Position { get; set; }
        public int Text_Font_Size { get; set; }
        public int Text_Font_Style { get; set; }
        public string Text_Colour_Code { get; set; }
        public int Info_Font_Size { get; set; }
        public int Info_Font_Style { get; set; }
        public string Info_Colour_Code { get; set; }
        public string Created_By { get; set; }
        public System.DateTime Created_Date_Time { get; set; }
        public string Modified_By { get; set; }
        public System.DateTime Modified_Date_Time { get; set; }
    }
}
