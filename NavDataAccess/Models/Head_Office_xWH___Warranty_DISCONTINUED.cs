//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace NavDataAccess.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Head_Office_xWH___Warranty_DISCONTINUED
    {
        public byte[] timestamp { get; set; }
        public string Id { get; set; }
        public System.DateTime Created_Date { get; set; }
        public string Product_Name { get; set; }
        public string Area_Limit { get; set; }
        public System.DateTime Purchase_Date { get; set; }
        public System.DateTime End_Date { get; set; }
        public System.DateTime Start_Date { get; set; }
        public string Warranty_URL { get; set; }
        public string Sale_Line { get; set; }
        public string Retail_Note { get; set; }
        public string Store_Name { get; set; }
        public string Item_No_ { get; set; }
        public string Serial_No_ { get; set; }
        public string Receipt_No_ { get; set; }
    }
}
