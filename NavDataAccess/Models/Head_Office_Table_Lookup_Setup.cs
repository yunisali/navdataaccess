//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace NavDataAccess.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Head_Office_Table_Lookup_Setup
    {
        public byte[] timestamp { get; set; }
        public int Table_No_ { get; set; }
        public int Lookup_Field_No_ { get; set; }
        public int Description_Field_No_ { get; set; }
        public int Filter_Field_No_ { get; set; }
        public int Filter_Table_No_ { get; set; }
        public int Item_Field_No_ { get; set; }
        public int Sales__Qty___Field_No_ { get; set; }
        public int Sales__LCY__Field_No_ { get; set; }
        public int COGS__LCY__Field_No_ { get; set; }
        public int Purchase__Qty___Field_No_ { get; set; }
        public int Purchase__LCY__Field_No_ { get; set; }
        public int Location_Filter_Field_No_ { get; set; }
        public int Date_Filter_Field_No_ { get; set; }
        public int Global_Dim_2_Filter_Field_No_ { get; set; }
    }
}
