//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace NavDataAccess.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Head_Office_Safe_Statement_Line
    {
        public byte[] timestamp { get; set; }
        public string Statement_No_ { get; set; }
        public int Line_No_ { get; set; }
        public string Statement_Code { get; set; }
        public string Staff_ID { get; set; }
        public int Transaction_Type { get; set; }
        public string POS_Terminal_No_ { get; set; }
        public string Bag_No_ { get; set; }
        public string Tender_Type { get; set; }
        public string Currency_Code { get; set; }
        public decimal Amount { get; set; }
        public decimal Amount_in_LCY { get; set; }
        public decimal Real_Exchange_Rate { get; set; }
        public System.DateTime Posted_Date { get; set; }
        public string Description { get; set; }
        public decimal Trans__Amount_in_LCY { get; set; }
        public decimal Trans__Amount { get; set; }
        public decimal Difference_in_LCY { get; set; }
        public decimal Difference_Amount { get; set; }
        public string Store_No_ { get; set; }
        public int Replication_Counter { get; set; }
        public int Bal__Account_Type { get; set; }
        public string Bal__Account_No_ { get; set; }
        public string Bal__Account_Name { get; set; }
        public string Safe_No_ { get; set; }
        public int Safe_Ledger_Entry_No_ { get; set; }
    }
}
