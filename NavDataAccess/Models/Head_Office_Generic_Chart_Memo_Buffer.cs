//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace NavDataAccess.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Head_Office_Generic_Chart_Memo_Buffer
    {
        public byte[] timestamp { get; set; }
        public string Code { get; set; }
        public string Language_Code { get; set; }
        public string Memo1 { get; set; }
        public string Memo2 { get; set; }
        public string Memo3 { get; set; }
        public string Memo4 { get; set; }
        public string Memo5 { get; set; }
        public string Memo6 { get; set; }
        public string Memo7 { get; set; }
        public string Memo8 { get; set; }
        public string Memo9 { get; set; }
        public string Memo10 { get; set; }
    }
}
