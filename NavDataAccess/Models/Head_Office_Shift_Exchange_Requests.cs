//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace NavDataAccess.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Head_Office_Shift_Exchange_Requests
    {
        public byte[] timestamp { get; set; }
        public string Request_By { get; set; }
        public System.DateTime Shift_Date { get; set; }
        public string Work_Location { get; set; }
        public System.DateTime Date_Requested { get; set; }
        public string Work_Shift { get; set; }
        public string Work_Role { get; set; }
        public string Request_To { get; set; }
        public System.DateTime Request_To_Date { get; set; }
        public string Request_To_Shift { get; set; }
        public string Request_To_Role { get; set; }
        public byte Accepted { get; set; }
        public byte Confirmed { get; set; }
        public System.DateTime Confirmed_Date { get; set; }
    }
}
