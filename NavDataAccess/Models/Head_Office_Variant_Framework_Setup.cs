//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace NavDataAccess.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Head_Office_Variant_Framework_Setup
    {
        public byte[] timestamp { get; set; }
        public string Framework_Code { get; set; }
        public string Description { get; set; }
        public int Registration_Type { get; set; }
        public string Item_Category { get; set; }
        public string Product_Group { get; set; }
        public string Vendor { get; set; }
        public int Variant_Pricing { get; set; }
        public int Barcoding { get; set; }
        public int Usage_of_Stock_Keeping_Units { get; set; }
        public string Barcode_Mask { get; set; }
        public string Variant_Suffix_Sequence_No_ { get; set; }
        public byte Use_Pop_up_Window { get; set; }
    }
}
