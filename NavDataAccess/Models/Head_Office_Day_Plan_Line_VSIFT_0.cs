//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace NavDataAccess.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Head_Office_Day_Plan_Line_VSIFT_0
    {
        public string Menu_Code { get; set; }
        public string Store_No_ { get; set; }
        public System.DateTime Date { get; set; }
        public int Line_No_ { get; set; }
        public Nullable<long> C_Cnt { get; set; }
        public Nullable<decimal> SUM_Actual_Cost { get; set; }
        public Nullable<decimal> SUM_Net_Amount { get; set; }
        public Nullable<decimal> SUM_Actual_Net_Amount { get; set; }
        public Nullable<decimal> SUM_Actual_Qty__Produced { get; set; }
        public Nullable<decimal> SUM_Actual_Labor_Cost { get; set; }
        public Nullable<decimal> SUM_Actual_Profit__LCY_ { get; set; }
    }
}
