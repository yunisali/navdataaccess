//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace NavDataAccess.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Head_Office_VAT_Report_Line
    {
        public byte[] timestamp { get; set; }
        public string VAT_Report_No_ { get; set; }
        public int Line_No_ { get; set; }
        public string Gen__Prod__Posting_Group { get; set; }
        public System.DateTime Posting_Date { get; set; }
        public string Document_No_ { get; set; }
        public int Document_Type { get; set; }
        public int Type { get; set; }
        public decimal Base { get; set; }
        public decimal Amount { get; set; }
        public int VAT_Calculation_Type { get; set; }
        public string Bill_to_Pay_to_No_ { get; set; }
        public byte EU_3_Party_Trade { get; set; }
        public string Source_Code { get; set; }
        public string Reason_Code { get; set; }
        public string Country_Region_Code { get; set; }
        public string Internal_Ref__No_ { get; set; }
        public decimal Unrealized_Amount { get; set; }
        public decimal Unrealized_Base { get; set; }
        public string External_Document_No_ { get; set; }
        public string VAT_Bus__Posting_Group { get; set; }
        public string VAT_Prod__Posting_Group { get; set; }
        public string VAT_Registration_No_ { get; set; }
        public string Gen__Bus__Posting_Group { get; set; }
        public string Record_Identifier { get; set; }
    }
}
