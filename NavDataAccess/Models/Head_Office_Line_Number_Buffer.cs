//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace NavDataAccess.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Head_Office_Line_Number_Buffer
    {
        public byte[] timestamp { get; set; }
        public int Old_Line_Number { get; set; }
        public int New_Line_Number { get; set; }
    }
}
