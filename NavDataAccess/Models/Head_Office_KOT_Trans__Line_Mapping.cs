//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace NavDataAccess.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Head_Office_KOT_Trans__Line_Mapping
    {
        public byte[] timestamp { get; set; }
        public string KOT_No_ { get; set; }
        public int KOT_Line_No_ { get; set; }
        public int Line_No_ { get; set; }
        public string Order_ID { get; set; }
        public string Receipt_No_ { get; set; }
        public int POS_Trans__Line_No_ { get; set; }
        public byte Voided { get; set; }
        public decimal Quantity { get; set; }
        public string Moved_From_Receipt_No_ { get; set; }
        public string Moved_From_Order_ID { get; set; }
        public int Moved_from_Trans__Line_No_ { get; set; }
        public byte Partial_Transfer { get; set; }
        public string Time_Modifier_Code { get; set; }
        public string Time_Modifier_Subcode { get; set; }
        public string Time_Modifier_Description { get; set; }
        public byte Modified__Internal_Use_Only_ { get; set; }
    }
}
