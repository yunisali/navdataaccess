//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace NavDataAccess.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class pt_cpuloadhistory
    {
        public Nullable<byte> SQL_Server_CPU__ { get; set; }
        public Nullable<byte> Other_Processes_CPU__ { get; set; }
        public Nullable<byte> Idle_CPU__ { get; set; }
        public System.DateTime Sample_Time { get; set; }
    }
}
