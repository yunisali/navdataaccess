//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace NavDataAccess.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Head_Office_Retail_Msg_Detail_Recipient
    {
        public byte[] timestamp { get; set; }
        public string Message_No_ { get; set; }
        public int Line_No_ { get; set; }
        public int Recipient_Type { get; set; }
        public string Recipient_ID { get; set; }
        public string Recipient_Group { get; set; }
        public string Recipient_Name { get; set; }
        public System.DateTime Date_and_Time_Received { get; set; }
        public System.DateTime Date_and_Time_Read { get; set; }
        public System.DateTime Date_and_Time_Processed { get; set; }
        public int Recipient_Status { get; set; }
        public System.DateTime Date_and_Time_Canceled { get; set; }
    }
}
