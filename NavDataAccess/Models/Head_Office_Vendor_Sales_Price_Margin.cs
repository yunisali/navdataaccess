//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace NavDataAccess.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Head_Office_Vendor_Sales_Price_Margin
    {
        public byte[] timestamp { get; set; }
        public string Vendor_No_ { get; set; }
        public string Division_Code { get; set; }
        public string Item_Category_Code { get; set; }
        public string Product_Group_Code { get; set; }
        public decimal Suggested_Retail_Price_Factor { get; set; }
        public decimal Purchase_Price_Factor { get; set; }
    }
}
