//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace NavDataAccess.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Head_Office_Dining_Table_Design_Location
    {
        public byte[] timestamp { get; set; }
        public string Dining_Area_Profile_ID { get; set; }
        public string Dining_Area_Layout_Code { get; set; }
        public int Dining_Table_No_ { get; set; }
        public int X1_Position__Design_ { get; set; }
        public int X2_Position__Design_ { get; set; }
        public int Y1_Position__Design_ { get; set; }
        public int Y2_Position__Design_ { get; set; }
        public int Joined_To_Table__Design_ { get; set; }
        public byte Dining_Tables_Joined__Design_ { get; set; }
        public int Joined_To_Master_Tbl___Design_ { get; set; }
    }
}
