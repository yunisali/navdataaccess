//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace NavDataAccess.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Head_Office_Shipping_Agent_Services
    {
        public byte[] timestamp { get; set; }
        public string Shipping_Agent_Code { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public string Shipping_Time { get; set; }
        public string Base_Calendar_Code { get; set; }
        public int Export_Type { get; set; }
        public string Export_File_Location { get; set; }
        public string External_Code_1 { get; set; }
        public string External_Code_2 { get; set; }
        public string External_Code_3 { get; set; }
        public string External_Code_4 { get; set; }
        public string External_Code_5 { get; set; }
        public string External_Code_6 { get; set; }
        public string External_Code_7 { get; set; }
    }
}
