//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace NavDataAccess.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Head_Office_MobileNAV_User_Key_Selection
    {
        public byte[] timestamp { get; set; }
        public string User_ID { get; set; }
        public int Page_No_ { get; set; }
        public int Key_No_ { get; set; }
        public byte Ascending { get; set; }
    }
}
