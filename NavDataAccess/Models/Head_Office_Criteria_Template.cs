//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace NavDataAccess.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Head_Office_Criteria_Template
    {
        public byte[] timestamp { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public int Table_ID { get; set; }
        public int Criteria_ID { get; set; }
        public string Created_By { get; set; }
        public System.DateTime Created_Date_Time { get; set; }
        public string Modified_By { get; set; }
        public System.DateTime Modified_Date_Time { get; set; }
    }
}
