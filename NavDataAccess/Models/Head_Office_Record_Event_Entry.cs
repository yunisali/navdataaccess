//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace NavDataAccess.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Head_Office_Record_Event_Entry
    {
        public byte[] timestamp { get; set; }
        public System.Guid GUID_No_ { get; set; }
        public System.DateTime Event_DateTime { get; set; }
        public int Event_Type { get; set; }
        public int Table_No_ { get; set; }
        public string Position_Text { get; set; }
        public int Old_Table_No_ { get; set; }
        public string Old_Position_Text { get; set; }
    }
}
