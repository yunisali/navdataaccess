//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace NavDataAccess.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Head_Office_DO_Payment_Trans__Log_Entry
    {
        public byte[] timestamp { get; set; }
        public int Entry_No_ { get; set; }
        public int Document_Type { get; set; }
        public string Document_No_ { get; set; }
        public string Customer_No_ { get; set; }
        public string Credit_Card_No_ { get; set; }
        public int Transaction_Type { get; set; }
        public int Transaction_Result { get; set; }
        public string Transaction_Description { get; set; }
        public decimal Amount { get; set; }
        public System.DateTime Transaction_Date_Time { get; set; }
        public int Transaction_Status { get; set; }
        public int Cust__Ledger_Entry_No_ { get; set; }
        public string Currency_Code { get; set; }
        public System.Guid Transaction_GUID { get; set; }
        public string Transaction_ID { get; set; }
        public string User_ID { get; set; }
        public int Parent_Entry_No_ { get; set; }
        public System.Guid Reference_GUID { get; set; }
    }
}
