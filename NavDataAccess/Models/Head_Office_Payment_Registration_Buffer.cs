//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace NavDataAccess.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Head_Office_Payment_Registration_Buffer
    {
        public byte[] timestamp { get; set; }
        public int Ledger_Entry_No_ { get; set; }
        public string Source_No_ { get; set; }
        public int Document_Type { get; set; }
        public string Document_No_ { get; set; }
        public string Description { get; set; }
        public System.DateTime Due_Date { get; set; }
        public string Name { get; set; }
        public decimal Remaining_Amount { get; set; }
        public byte Payment_Made { get; set; }
        public System.DateTime Date_Received { get; set; }
        public decimal Amount_Received { get; set; }
        public decimal Original_Remaining_Amount { get; set; }
        public decimal Rem__Amt__after_Discount { get; set; }
        public System.DateTime Pmt__Discount_Date { get; set; }
    }
}
