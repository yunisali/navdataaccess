//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace NavDataAccess.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Head_Office_Store_Capacity
    {
        public byte[] timestamp { get; set; }
        public string Location_Code { get; set; }
        public string Item_Category_Code { get; set; }
        public string Product_Group_Code { get; set; }
        public string Season { get; set; }
        public decimal Goal { get; set; }
    }
}
