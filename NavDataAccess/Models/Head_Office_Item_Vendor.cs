//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace NavDataAccess.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Head_Office_Item_Vendor
    {
        public byte[] timestamp { get; set; }
        public string Vendor_No_ { get; set; }
        public string Item_No_ { get; set; }
        public string Variant_Code { get; set; }
        public string Lead_Time_Calculation { get; set; }
        public string Vendor_Item_No_ { get; set; }
    }
}
