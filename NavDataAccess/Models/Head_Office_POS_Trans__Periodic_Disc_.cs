//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace NavDataAccess.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Head_Office_POS_Trans__Periodic_Disc_
    {
        public byte[] timestamp { get; set; }
        public string Receipt_No_ { get; set; }
        public int Line_No_ { get; set; }
        public int No_ { get; set; }
        public int Entry_Status { get; set; }
        public int DiscType { get; set; }
        public decimal Discount__ { get; set; }
        public decimal Discount_Amount { get; set; }
        public int Periodic_Disc__Type { get; set; }
        public string Periodic_Disc__Group { get; set; }
        public string POS_Terminal_No_ { get; set; }
        public string Offer_No_ { get; set; }
        public string Member_Attribute { get; set; }
        public string Member_Attribute_Value { get; set; }
        public string Tracking_No_ { get; set; }
        public decimal Points { get; set; }
        public int Tracking_Instance_ID { get; set; }
        public decimal Benefit_Step_Amount { get; set; }
        public int Sequence_Code { get; set; }
        public int Sequence_Function { get; set; }
        public byte Manual_Selection { get; set; }
        public string Line_Discount_Group { get; set; }
        public decimal Coupon_Quantity { get; set; }
        public decimal Coupon_Discount__ { get; set; }
        public decimal Coupon_Discount_Amount { get; set; }
        public string Coupon_Code { get; set; }
        public decimal Coupon_Tender_Amount { get; set; }
        public int Coupon_POS_Trans__Line_No_ { get; set; }
        public int Mix___Match_Line_No_ { get; set; }
        public int Tot__Disc_Info_Line_No_ { get; set; }
        public decimal Total_Disc__Amount { get; set; }
        public decimal Periodic_Discount_Amount { get; set; }
        public decimal Total_Disc___ { get; set; }
        public byte Block_Manual_Price_Change { get; set; }
        public byte Block_Line_Discount_Offer { get; set; }
        public byte Block_Total_Discount_Offer { get; set; }
        public byte Block_Tender_Type_Discount { get; set; }
        public byte Block_Loyalty_Points { get; set; }
        public byte Block_Infocode_Discount { get; set; }
    }
}
