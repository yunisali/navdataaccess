//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace NavDataAccess.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Head_Office_Generic_Chart_Setup
    {
        public byte[] timestamp { get; set; }
        public string ID { get; set; }
        public int Source_Type { get; set; }
        public string Name { get; set; }
        public string Title { get; set; }
        public string Filter_Text { get; set; }
        public int Type { get; set; }
        public int Source_ID { get; set; }
        public string Object_Name { get; set; }
        public int X_Axis_Field_ID { get; set; }
        public string X_Axis_Field_Name { get; set; }
        public string X_Axis_Field_Caption { get; set; }
        public string X_Axis_Title { get; set; }
        public byte X_Axis_Show_Title { get; set; }
        public string Y_Axis_Title { get; set; }
        public byte Y_Axis_Show_Title { get; set; }
        public int Z_Axis_Field_ID { get; set; }
        public string Z_Axis_Field_Name { get; set; }
        public string Z_Axis_Field_Caption { get; set; }
        public string Z_Axis_Title { get; set; }
        public byte Z_Axis_Show_Title { get; set; }
    }
}
