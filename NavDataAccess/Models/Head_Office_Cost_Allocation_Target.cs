//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace NavDataAccess.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Head_Office_Cost_Allocation_Target
    {
        public byte[] timestamp { get; set; }
        public string ID { get; set; }
        public int Line_No_ { get; set; }
        public string Target_Cost_Type { get; set; }
        public string Target_Cost_Center { get; set; }
        public string Target_Cost_Object { get; set; }
        public decimal Static_Base { get; set; }
        public decimal Static_Weighting { get; set; }
        public decimal Share { get; set; }
        public decimal Percent { get; set; }
        public string Comment { get; set; }
        public int Base { get; set; }
        public string No__Filter { get; set; }
        public string Cost_Center_Filter { get; set; }
        public string Cost_Object_Filter { get; set; }
        public int Date_Filter_Code { get; set; }
        public string Group_Filter { get; set; }
        public int Allocation_Target_Type { get; set; }
        public decimal Percent_per_Share { get; set; }
        public decimal Amount_per_Share { get; set; }
        public System.DateTime Share_Updated_on { get; set; }
        public System.DateTime Last_Date_Modified { get; set; }
        public string User_ID { get; set; }
    }
}
