//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace NavDataAccess.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Head_Office_Batch_Import_Paths
    {
        public byte[] timestamp { get; set; }
        public string User { get; set; }
        public int Entry_Type { get; set; }
        public string Import_Path_Filename { get; set; }
    }
}
