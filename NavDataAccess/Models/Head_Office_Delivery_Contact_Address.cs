//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace NavDataAccess.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Head_Office_Delivery_Contact_Address
    {
        public byte[] timestamp { get; set; }
        public string Phone_No_ { get; set; }
        public int Address_Type { get; set; }
        public string Street_Name { get; set; }
        public string Street_No_ { get; set; }
        public string Post_Code { get; set; }
        public string City { get; set; }
        public string Address_2 { get; set; }
        public string Directions { get; set; }
        public string Restaurant_No_ { get; set; }
        public string Grid { get; set; }
        public System.DateTime Date_Created { get; set; }
        public System.DateTime Last_Date_Modified { get; set; }
        public string Building_Letter { get; set; }
        public string Building_Name { get; set; }
        public string Building_Number { get; set; }
        public string Country_Region_Code { get; set; }
        public string Country_IsoCode { get; set; }
        public string District { get; set; }
        public string Intersection { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public string Region { get; set; }
        public string Room_Number { get; set; }
        public string Territory_Code { get; set; }
    }
}
