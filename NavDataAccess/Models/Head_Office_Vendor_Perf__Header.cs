//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace NavDataAccess.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Head_Office_Vendor_Perf__Header
    {
        public byte[] timestamp { get; set; }
        public string Purchase_Order_No_ { get; set; }
        public string Vendor_No_ { get; set; }
        public System.DateTime Posting_Date { get; set; }
        public System.DateTime Expected_Receipt_Date { get; set; }
        public System.DateTime Actual_Receipt_Date { get; set; }
        public System.DateTime Order_Release_Date { get; set; }
        public System.DateTime Order_Release_Time { get; set; }
        public string Released_By_User { get; set; }
        public System.DateTime Promised_Receipt_Date { get; set; }
        public byte Order_Fully_Recvd_and_Invoiced { get; set; }
    }
}
