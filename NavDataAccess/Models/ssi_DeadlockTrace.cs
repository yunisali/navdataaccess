//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace NavDataAccess.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class ssi_DeadlockTrace
    {
        public long entry_no { get; set; }
        public System.DateTime DeadlockTime { get; set; }
        public string VictimProcessID { get; set; }
        public string VictimLoginName { get; set; }
        public string VictimHostName { get; set; }
        public string VictimClientApp { get; set; }
        public string VictimLastBatchStarted { get; set; }
        public string VictimLockMode { get; set; }
        public string VictimIsolationLevel { get; set; }
        public string VictimWaitResource { get; set; }
        public string VictimObjName { get; set; }
        public string VictimLockModeHeld { get; set; }
        public string LiveLockModeRequest { get; set; }
        public string VictimProcName { get; set; }
        public string VictimExecStack { get; set; }
        public string VictimInputBuffer { get; set; }
        public string LiveProcessID { get; set; }
        public string LiveLoginName { get; set; }
        public string LiveHostName { get; set; }
        public string LiveClientApp { get; set; }
        public string LiveLastBatchStarted { get; set; }
        public string LiveLockMode { get; set; }
        public string LiveIsolationLevel { get; set; }
        public string LiveWaitResource { get; set; }
        public string LiveObjName { get; set; }
        public string LiveLockModeHeld { get; set; }
        public string VictimLockModeRequest { get; set; }
        public string LiveProcName { get; set; }
        public string LiveExecStack { get; set; }
        public string LiveInputBuffer { get; set; }
    }
}
