//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace NavDataAccess.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Head_Office_Service_Mgt__Setup
    {
        public byte[] timestamp { get; set; }
        public string Primary_Key { get; set; }
        public int Fault_Reporting_Level { get; set; }
        public byte Link_Service_to_Service_Item { get; set; }
        public byte Salesperson_Mandatory { get; set; }
        public decimal Warranty_Disc_____Parts_ { get; set; }
        public decimal Warranty_Disc_____Labor_ { get; set; }
        public byte Contract_Rsp__Time_Mandatory { get; set; }
        public string Service_Order_Starting_Fee { get; set; }
        public byte Register_Contract_Changes { get; set; }
        public string Contract_Inv__Line_Text_Code { get; set; }
        public string Contract_Line_Inv__Text_Code { get; set; }
        public string Contract_Inv__Period_Text_Code { get; set; }
        public string Contract_Credit_Line_Text_Code { get; set; }
        public string Send_First_Warning_To { get; set; }
        public string Send_Second_Warning_To { get; set; }
        public string Send_Third_Warning_To { get; set; }
        public decimal First_Warning_Within__Hours_ { get; set; }
        public decimal Second_Warning_Within__Hours_ { get; set; }
        public decimal Third_Warning_Within__Hours_ { get; set; }
        public int Next_Service_Calc__Method { get; set; }
        public byte Service_Order_Type_Mandatory { get; set; }
        public int Service_Zones_Option { get; set; }
        public byte Service_Order_Start_Mandatory { get; set; }
        public byte Service_Order_Finish_Mandatory { get; set; }
        public int Resource_Skills_Option { get; set; }
        public byte One_Service_Item_Line_Order { get; set; }
        public byte Unit_of_Measure_Mandatory { get; set; }
        public byte Fault_Reason_Code_Mandatory { get; set; }
        public int Contract_Serv__Ord___Max__Days { get; set; }
        public System.DateTime Last_Contract_Service_Date { get; set; }
        public byte Work_Type_Code_Mandatory { get; set; }
        public int Logo_Position_on_Documents { get; set; }
        public byte Use_Contract_Cancel_Reason { get; set; }
        public decimal Default_Response_Time__Hours_ { get; set; }
        public string Default_Warranty_Duration { get; set; }
        public string Service_Invoice_Nos_ { get; set; }
        public string Contract_Invoice_Nos_ { get; set; }
        public string Service_Item_Nos_ { get; set; }
        public string Service_Order_Nos_ { get; set; }
        public string Service_Contract_Nos_ { get; set; }
        public string Contract_Template_Nos_ { get; set; }
        public string Troubleshooting_Nos_ { get; set; }
        public string Prepaid_Posting_Document_Nos_ { get; set; }
        public string Loaner_Nos_ { get; set; }
        public string Serv__Job_Responsibility_Code { get; set; }
        public int Contract_Value_Calc__Method { get; set; }
        public decimal Contract_Value__ { get; set; }
        public string Service_Quote_Nos_ { get; set; }
        public string Posted_Service_Invoice_Nos_ { get; set; }
        public string Posted_Serv__Credit_Memo_Nos_ { get; set; }
        public string Posted_Service_Shipment_Nos_ { get; set; }
        public byte Shipment_on_Invoice { get; set; }
        public byte Copy_Comments_Order_to_Invoice { get; set; }
        public byte Copy_Comments_Order_to_Shpt_ { get; set; }
        public string Service_Credit_Memo_Nos_ { get; set; }
        public byte Copy_Time_Sheet_to_Order { get; set; }
        public string Base_Calendar_Code { get; set; }
        public string Contract_Credit_Memo_Nos_ { get; set; }
    }
}
