//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace NavDataAccess.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Head_Office_Enhanced_List_Data
    {
        public byte[] timestamp { get; set; }
        public string Enhanced_List_Code { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public int Data_From { get; set; }
        public string Data_Code { get; set; }
        public int Sequence { get; set; }
        public byte Default { get; set; }
        public byte Blocked { get; set; }
        public int Factbox_Field_Limit { get; set; }
        public byte Depending_on_Current_Record { get; set; }
        public int Table_ID { get; set; }
        public string Table_Filter { get; set; }
        public string Created_By { get; set; }
        public System.DateTime Created_Date_Time { get; set; }
        public string Modified_By { get; set; }
        public System.DateTime Modified_Date_Time { get; set; }
    }
}
