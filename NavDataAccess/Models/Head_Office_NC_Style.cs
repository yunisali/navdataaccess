//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace NavDataAccess.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Head_Office_NC_Style
    {
        public byte[] timestamp { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public byte Bold { get; set; }
        public byte Italic { get; set; }
        public byte Underline { get; set; }
        public string Colour_Code { get; set; }
        public int Font_Size { get; set; }
        public string Font_Family { get; set; }
        public string Margin { get; set; }
        public string Padding { get; set; }
        public int Border_Style { get; set; }
        public int Border_Width { get; set; }
        public string Border_Colour_Code { get; set; }
        public string Width { get; set; }
        public string Height { get; set; }
        public int Horizontal_Align { get; set; }
        public int Vertical_Align { get; set; }
        public string Additional_Attributes { get; set; }
        public int Border_Collapse { get; set; }
        public int Border_Spacing { get; set; }
        public string Background_Colour_Code { get; set; }
        public string Created_By { get; set; }
        public System.DateTime Created_Date_Time { get; set; }
        public string Modified_By { get; set; }
        public System.DateTime Modified_Date_Time { get; set; }
    }
}
