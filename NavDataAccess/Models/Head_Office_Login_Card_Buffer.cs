//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace NavDataAccess.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Head_Office_Login_Card_Buffer
    {
        public byte[] timestamp { get; set; }
        public string ClubCode { get; set; }
        public string CardNo { get; set; }
        public string ClubDescription { get; set; }
        public string CardholderName { get; set; }
        public byte Blocked { get; set; }
    }
}
