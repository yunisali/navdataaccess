//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace NavDataAccess.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Head_Office_Batch_Posting_Setup
    {
        public byte[] timestamp { get; set; }
        public string ID { get; set; }
        public int Run_Mode { get; set; }
        public byte Batch_Posting_Started { get; set; }
        public byte Use_Batch_Posting { get; set; }
        public int Wait_between_Tasks__sec_ { get; set; }
        public System.DateTime Start_Date { get; set; }
        public System.DateTime Start_Time { get; set; }
        public System.DateTime Last_Run_Date { get; set; }
        public System.DateTime Last_Run_Time { get; set; }
        public string Error_E_mail_Address { get; set; }
        public string Error_CC_E_mail_Address { get; set; }
        public int Number_of_Attempts { get; set; }
        public string Time_Stamp_Path { get; set; }
        public byte Error_Monitor { get; set; }
        public int Error_Duration__min_ { get; set; }
        public int No__of_Lines_in_Journals { get; set; }
        public decimal Max_Average_Proc__Time { get; set; }
        public decimal Min__Average_Proc__Time { get; set; }
        public int Days_Finished_Job_Exists { get; set; }
        public int Running_Method { get; set; }
        public System.DateTime Manual_Shutdown_Time { get; set; }
    }
}
