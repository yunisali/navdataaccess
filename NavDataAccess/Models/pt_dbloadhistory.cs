//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace NavDataAccess.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class pt_dbloadhistory
    {
        public long sample { get; set; }
        public int id { get; set; }
        public System.DateTime sample_time { get; set; }
        public int dbid { get; set; }
        public long data_mb { get; set; }
        public long delta_data_mb { get; set; }
        public long log_mb { get; set; }
        public long delta_log_mb { get; set; }
        public long io { get; set; }
        public long delta_io { get; set; }
        public long io_bytes_mb { get; set; }
        public long delta_io_bytes_mb { get; set; }
        public long cpu_s { get; set; }
        public long delta_cpu_s { get; set; }
        public long cache_bytes_mb { get; set; }
        public long delta_cache_bytes_mb { get; set; }
    }
}
