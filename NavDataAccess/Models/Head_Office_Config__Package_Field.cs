//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace NavDataAccess.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Head_Office_Config__Package_Field
    {
        public byte[] timestamp { get; set; }
        public string Package_Code { get; set; }
        public int Table_ID { get; set; }
        public int Field_ID { get; set; }
        public string Field_Name { get; set; }
        public string Field_Caption { get; set; }
        public byte Validate_Field { get; set; }
        public byte Include_Field { get; set; }
        public byte Localize_Field { get; set; }
        public int Relation_Table_ID { get; set; }
        public byte Dimension { get; set; }
        public byte Primary_Key { get; set; }
        public int Processing_Order { get; set; }
        public byte Create_Missing_Codes { get; set; }
    }
}
