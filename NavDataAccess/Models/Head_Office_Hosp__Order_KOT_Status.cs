//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace NavDataAccess.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Head_Office_Hosp__Order_KOT_Status
    {
        public byte[] timestamp { get; set; }
        public string KOT_No_ { get; set; }
        public string Receipt_No_ { get; set; }
        public int Status { get; set; }
        public string Staff_ID { get; set; }
        public int No__of_Routing_Lines { get; set; }
        public int No__of_Expeditor_Lines { get; set; }
        public int No__of_Started_Lines { get; set; }
        public int No__of_Bumped_Lines { get; set; }
        public int No__of_Bumped_Prep__Lines { get; set; }
        public int No__of_Bumped_Exp__Lines { get; set; }
        public byte Confirmed_by_Expeditor_Station { get; set; }
        public System.DateTime Sent_Date_Time { get; set; }
        public System.DateTime Started_Date_Time { get; set; }
        public System.DateTime Finished_Date_Time { get; set; }
        public System.DateTime Confirmed_Date_Time { get; set; }
        public System.DateTime Expected_Finished_Date_Time { get; set; }
        public System.DateTime Error_Date_Time { get; set; }
        public System.DateTime Date_Time_Created { get; set; }
        public System.DateTime Expected_Start_Date_Time { get; set; }
        public decimal On_Hold_Offset__Min__ { get; set; }
        public byte Transaction_Voided { get; set; }
        public byte Ready_to_be_Served__Manually_ { get; set; }
        public byte For_Printer_Station { get; set; }
        public string C_FONT_ID_ { get; set; }
        public string Restaurant_No_ { get; set; }
        public string Sales_Type { get; set; }
        public byte Delivery_Takeout_Order { get; set; }
        public byte Printed_for_Pre_Order { get; set; }
        public string Expected_after__Min__ { get; set; }
        public string Finished_Stations { get; set; }
        public byte Overdue { get; set; }
        public byte Paid_when_Served { get; set; }
    }
}
