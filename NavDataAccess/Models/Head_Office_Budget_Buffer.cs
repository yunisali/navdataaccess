//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace NavDataAccess.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Head_Office_Budget_Buffer
    {
        public byte[] timestamp { get; set; }
        public string G_L_Account_No_ { get; set; }
        public string Dimension_Value_Code_1 { get; set; }
        public string Dimension_Value_Code_2 { get; set; }
        public string Dimension_Value_Code_3 { get; set; }
        public string Dimension_Value_Code_4 { get; set; }
        public string Dimension_Value_Code_5 { get; set; }
        public string Dimension_Value_Code_6 { get; set; }
        public string Dimension_Value_Code_7 { get; set; }
        public string Dimension_Value_Code_8 { get; set; }
        public System.DateTime Date { get; set; }
        public decimal Amount { get; set; }
        public decimal Quantity { get; set; }
        public decimal Cost_Amount { get; set; }
        public decimal Discount_Amount { get; set; }
        public decimal Neg__Adjust__Amount { get; set; }
        public decimal Inventory_Amount { get; set; }
    }
}
