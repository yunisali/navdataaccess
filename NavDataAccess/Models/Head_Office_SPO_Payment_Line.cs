//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace NavDataAccess.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Head_Office_SPO_Payment_Line
    {
        public byte[] timestamp { get; set; }
        public int Document_Type { get; set; }
        public string Document_No_ { get; set; }
        public int Document_Line_No_ { get; set; }
        public int Line_No_ { get; set; }
        public decimal Amount { get; set; }
        public string Reference_No_ { get; set; }
        public int Origin { get; set; }
        public System.DateTime Date_Created { get; set; }
        public System.DateTime Time_Created { get; set; }
        public string User { get; set; }
        public string Receipt_No_ { get; set; }
    }
}
