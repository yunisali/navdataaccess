//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace NavDataAccess.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Head_Office_Member_Notification_Log
    {
        public byte[] timestamp { get; set; }
        public string Id { get; set; }
        public string ContactId { get; set; }
        public System.DateTime DateDisplayed { get; set; }
        public string DeviceId { get; set; }
        public System.DateTime DateClosed { get; set; }
        public int ReplicationCounter { get; set; }
        public int NotificationStatus { get; set; }
    }
}
