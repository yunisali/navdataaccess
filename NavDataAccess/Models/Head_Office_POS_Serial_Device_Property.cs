//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace NavDataAccess.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Head_Office_POS_Serial_Device_Property
    {
        public byte[] timestamp { get; set; }
        public string Device_ID { get; set; }
        public string Line_No_ { get; set; }
        public string Profile_ID { get; set; }
        public string Name { get; set; }
        public string Value { get; set; }
    }
}
