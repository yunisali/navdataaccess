//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace NavDataAccess.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Head_Office_Ask_nOra___Record_Alias
    {
        public byte[] timestamp { get; set; }
        public int Table_No_ { get; set; }
        public string Alias_Code { get; set; }
        public string Position_Text { get; set; }
        public string Primary_Key_1 { get; set; }
        public string Primary_Key_2 { get; set; }
        public string Primary_Key_3 { get; set; }
        public string Primary_Key_4 { get; set; }
        public string Primary_Key_5 { get; set; }
    }
}
