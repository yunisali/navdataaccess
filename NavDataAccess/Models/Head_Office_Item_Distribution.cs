//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace NavDataAccess.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Head_Office_Item_Distribution
    {
        public byte[] timestamp { get; set; }
        public int Type { get; set; }
        public string Code { get; set; }
        public string Item_No_ { get; set; }
        public int Status { get; set; }
        public string Description { get; set; }
        public string Search_Description { get; set; }
        public string Product_Group_Code { get; set; }
        public System.DateTime Obsolescence_Date { get; set; }
        public string Item_Category_Code { get; set; }
        public string Vendor_No_ { get; set; }
        public string Dimension_Pattern_Code { get; set; }
        public int Ordered_by { get; set; }
        public int Ordering_Method { get; set; }
        public int No__of_Facings { get; set; }
    }
}
