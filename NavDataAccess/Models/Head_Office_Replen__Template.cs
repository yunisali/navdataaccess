//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace NavDataAccess.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Head_Office_Replen__Template
    {
        public byte[] timestamp { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public int Replenishment_Type { get; set; }
        public string Location_Code { get; set; }
        public string Vendor_No__Filter { get; set; }
        public string Item_Category_Filter { get; set; }
        public string Item_Product_Group_Filter { get; set; }
        public string Item_No__Filter { get; set; }
        public int Purchase_Order_Type { get; set; }
        public string Replenishm__Calc__Type_Filter { get; set; }
        public string Buyer_Group_Filter { get; set; }
        public string Buyer_ID { get; set; }
        public string Buyer_Group_Code { get; set; }
        public int Create_Orders_Automatically { get; set; }
        public string Store_Group_Filter { get; set; }
        public string Season_Filter { get; set; }
        public string Item_Division_Filter { get; set; }
        public int ABC_Amount_Filter { get; set; }
        public int ABC_Profit_Filter { get; set; }
        public string Special_Group_Code_Filter { get; set; }
        public string Item_Attribute_Code_Filter { get; set; }
        public string Item_Attribute_Value_Filter { get; set; }
        public string Item_Hierarchy_Filter { get; set; }
        public int Item_Hierarchy_Level_Filter { get; set; }
        public string Item_Hierarchy_Value_Filter { get; set; }
    }
}
