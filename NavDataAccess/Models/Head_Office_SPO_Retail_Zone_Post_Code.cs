//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace NavDataAccess.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Head_Office_SPO_Retail_Zone_Post_Code
    {
        public byte[] timestamp { get; set; }
        public string Retail_Zone_Code { get; set; }
        public string Post_Code { get; set; }
        public string Description { get; set; }
    }
}
