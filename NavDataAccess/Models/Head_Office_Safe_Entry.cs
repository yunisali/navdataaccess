//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace NavDataAccess.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Head_Office_Safe_Entry
    {
        public byte[] timestamp { get; set; }
        public string Safe_Code { get; set; }
        public int Entry_No_ { get; set; }
        public int Type { get; set; }
        public decimal Amount { get; set; }
        public string POS_Terminal { get; set; }
        public string Staff_ID { get; set; }
        public System.DateTime Date { get; set; }
        public System.DateTime Time { get; set; }
        public string Store_No_ { get; set; }
        public string Shift_No_ { get; set; }
        public string Currency_Code { get; set; }
        public string Reason_Code { get; set; }
        public string User_ID { get; set; }
        public string External_Document_No_ { get; set; }
        public int Transaction_No_ { get; set; }
        public int Replication_Counter { get; set; }
        public string Bag_No_ { get; set; }
    }
}
