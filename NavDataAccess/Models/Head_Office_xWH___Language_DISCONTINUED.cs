//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace NavDataAccess.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Head_Office_xWH___Language_DISCONTINUED
    {
        public byte[] timestamp { get; set; }
        public int No_ { get; set; }
        public string English_Name { get; set; }
        public string Native_Name { get; set; }
        public string Culture { get; set; }
    }
}
