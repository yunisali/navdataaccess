//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace NavDataAccess.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Head_Office_WI_NC_Product_Manufactur_Mapp
    {
        public byte[] timestamp { get; set; }
        public string Item_No_ { get; set; }
        public string Manufacturer_Code { get; set; }
        public int Id { get; set; }
        public int ProductId { get; set; }
        public int ManufacturerId { get; set; }
        public byte IsFeaturedProduct { get; set; }
        public int DisplayOrder { get; set; }
        public byte Deleted { get; set; }
    }
}
