//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace NavDataAccess.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Head_Office_Value_Entry_VSIFT_18
    {
        public int Item_Ledger_Entry_Type { get; set; }
        public string Location_Code { get; set; }
        public string Item_Category { get; set; }
        public string Global_Dimension_2_Code { get; set; }
        public System.DateTime Posting_Date { get; set; }
        public Nullable<long> C_Cnt { get; set; }
        public Nullable<decimal> SUM_Invoiced_Quantity { get; set; }
        public Nullable<decimal> SUM_Sales_Amount__Actual_ { get; set; }
        public Nullable<decimal> SUM_Cost_Amount__Actual_ { get; set; }
    }
}
