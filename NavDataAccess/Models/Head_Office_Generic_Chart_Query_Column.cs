//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace NavDataAccess.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Head_Office_Generic_Chart_Query_Column
    {
        public byte[] timestamp { get; set; }
        public int Query_No_ { get; set; }
        public int Query_Column_No_ { get; set; }
        public int Entry_No_ { get; set; }
        public string Column_Name { get; set; }
        public int Column_Data_Type { get; set; }
        public int Column_Type { get; set; }
        public int Aggregation_Type { get; set; }
    }
}
