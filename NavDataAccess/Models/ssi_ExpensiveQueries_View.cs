//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace NavDataAccess.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class ssi_ExpensiveQueries_View
    {
        public string statement_text { get; set; }
        public long execution_count { get; set; }
        public Nullable<long> avg_logical_reads { get; set; }
        public Nullable<long> avg_cpu__msec_ { get; set; }
        public Nullable<long> avg_duration__msec_ { get; set; }
        public System.DateTime creation_time { get; set; }
        public System.DateTime last_execution_time { get; set; }
        public string cursor_type { get; set; }
        public string missing_index_impact { get; set; }
        public string missing_index_table { get; set; }
    }
}
