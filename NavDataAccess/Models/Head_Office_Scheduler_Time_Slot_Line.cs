//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace NavDataAccess.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Head_Office_Scheduler_Time_Slot_Line
    {
        public byte[] timestamp { get; set; }
        public string Time_Slot_Code { get; set; }
        public int Time_Slot_Line { get; set; }
        public System.DateTime Start_Time { get; set; }
    }
}
