//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace NavDataAccess.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Head_Office_Replen__Sales_Profile_Line
    {
        public byte[] timestamp { get; set; }
        public string Replenishm__Sales_Profile_Code { get; set; }
        public int Line_No_ { get; set; }
        public string Date_Formula_for_Start_Date { get; set; }
        public string Date_Formula_for_End_Date { get; set; }
        public decimal Weight { get; set; }
    }
}
