//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace NavDataAccess.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Head_Office_Credit_Trans_Re_export_History
    {
        public byte[] timestamp { get; set; }
        public int No_ { get; set; }
        public int Credit_Transfer_Register_No_ { get; set; }
        public System.DateTime Re_export_Date { get; set; }
        public string Re_exported_By { get; set; }
    }
}
