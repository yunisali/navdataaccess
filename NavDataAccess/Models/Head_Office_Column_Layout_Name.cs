//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace NavDataAccess.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Head_Office_Column_Layout_Name
    {
        public byte[] timestamp { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Analysis_View_Name { get; set; }
    }
}
