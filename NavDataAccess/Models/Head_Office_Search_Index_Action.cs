//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace NavDataAccess.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Head_Office_Search_Index_Action
    {
        public byte[] timestamp { get; set; }
        public System.Guid ID { get; set; }
        public int Action { get; set; }
        public string Parameter { get; set; }
        public string Executed_On { get; set; }
        public System.DateTime DateTime { get; set; }
        public string User { get; set; }
        public int Table_No_ { get; set; }
    }
}
