//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace NavDataAccess.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Head_Office_Detailed_Cust__Ledg__Entry_VSIFT_5
    {
        public string Customer_No_ { get; set; }
        public string Currency_Code { get; set; }
        public string Initial_Entry_Global_Dim__1 { get; set; }
        public string Initial_Entry_Global_Dim__2 { get; set; }
        public System.DateTime Initial_Entry_Due_Date { get; set; }
        public System.DateTime Posting_Date { get; set; }
        public Nullable<long> C_Cnt { get; set; }
        public Nullable<decimal> SUM_Amount { get; set; }
        public Nullable<decimal> SUM_Amount__LCY_ { get; set; }
    }
}
