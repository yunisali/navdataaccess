//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace NavDataAccess.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Head_Office_Accounting_Period_GB
    {
        public byte[] timestamp { get; set; }
        public int Period_Type { get; set; }
        public System.DateTime Period_Start { get; set; }
        public System.DateTime Period_End { get; set; }
        public int Period_No_ { get; set; }
        public string Period_Name { get; set; }
        public int Average_Cost_Calc__Type { get; set; }
        public int Average_Cost_Period { get; set; }
        public byte Closed { get; set; }
        public int Line_No_ { get; set; }
    }
}
