//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace NavDataAccess.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Head_Office_Info__Set_Temp_Record_Version
    {
        public byte[] timestamp { get; set; }
        public string Information_Set_Code { get; set; }
        public string Filter_Text { get; set; }
        public int Field_Limit { get; set; }
        public int Record_Set_ID { get; set; }
        public System.DateTime DateTime_Created { get; set; }
    }
}
