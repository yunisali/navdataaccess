//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace NavDataAccess.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Head_Office_Inventory_Masks
    {
        public byte[] timestamp { get; set; }
        public int Seq__No_ { get; set; }
        public string Templates { get; set; }
        public string Handheld_User { get; set; }
        public string Store_No_ { get; set; }
        public int Entry_Type { get; set; }
        public string Reason_Code { get; set; }
        public int Unit { get; set; }
        public string Vendor_No_ { get; set; }
        public string Description { get; set; }
        public int Journal_Type { get; set; }
        public byte Printing { get; set; }
        public byte Posting { get; set; }
        public int Type_of_Entering { get; set; }
        public string Shortcut_Dimension_2_Code { get; set; }
        public string New_Store_Code { get; set; }
        public string Source_Code { get; set; }
        public byte Product_Strict { get; set; }
        public byte Accepted { get; set; }
        public string Location { get; set; }
        public string New_Location { get; set; }
        public string Batch { get; set; }
        public byte Use_Variants { get; set; }
        public string Shortcut_Dimension_1_Code { get; set; }
        public int Pre_Process { get; set; }
        public int Pre_Process_Object_ID { get; set; }
        public int Close_Process { get; set; }
        public int Close_Process_Object_ID { get; set; }
        public int Confirm_Codeunit { get; set; }
        public int Report_Object_ID { get; set; }
        public string Product_Group_Filter { get; set; }
        public string Inv__Posting_Gr__Filter { get; set; }
        public byte Use_Area { get; set; }
        public int Handheld_Type { get; set; }
        public int Blocked_for_RF { get; set; }
        public int Leading_Behavior { get; set; }
        public int Needs_to_Be_in_Distribution { get; set; }
        public int Needs_to_Be_in_Worksheet { get; set; }
        public int Needs_to_Be_Ordered_by_Hand { get; set; }
        public int Needs_to_Be_Ordered_at_Store { get; set; }
        public int Search_for_Item_by { get; set; }
        public int Quantity_Method { get; set; }
        public int Order_Date_Type { get; set; }
        public string Order_Date_Calculation { get; set; }
        public int End_Date_Type { get; set; }
        public string End_Date_Calculation { get; set; }
        public int Change_Vendor_in_Line { get; set; }
        public int Change_UOM_in_Line { get; set; }
        public int Item_Check { get; set; }
        public decimal Quick_default_Quantity { get; set; }
        public string Inv__Adjust__Group { get; set; }
        public int Vendor_to_Use_in_Returns { get; set; }
        public string Return_Reason_Code { get; set; }
        public string Item_Journal_Doc_No_ { get; set; }
        public int Status { get; set; }
        public byte Use_Batch_Posting { get; set; }
        public int Order_Status { get; set; }
        public int Standalone_Store_Action { get; set; }
        public string Document_Group { get; set; }
    }
}
