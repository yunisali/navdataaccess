//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace NavDataAccess.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Head_Office_SMS_Message_log
    {
        public byte[] timestamp { get; set; }
        public int Entry_No_ { get; set; }
        public System.DateTime Date_Time { get; set; }
        public string Message { get; set; }
        public string Url { get; set; }
        public string Phone_Numbers { get; set; }
        public string Status_Code { get; set; }
        public string Status_Description { get; set; }
    }
}
