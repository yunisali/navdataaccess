//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace NavDataAccess.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Head_Office_Posted_P_R_Counting_Lines
    {
        public byte[] timestamp { get; set; }
        public string Document_No_ { get; set; }
        public int Line_No_ { get; set; }
        public string Item_No_ { get; set; }
        public string Description { get; set; }
        public string Variant_Code { get; set; }
        public string Barcode { get; set; }
        public decimal Quantity { get; set; }
        public string Unit_of_Measure_Code { get; set; }
        public decimal Qty__per_Unit_of_Measure { get; set; }
        public decimal Ordered_Qty_ { get; set; }
        public decimal Quantity__base_ { get; set; }
        public decimal Ordered_Qty___base_ { get; set; }
        public string Shortcut_Dimension_1_Code { get; set; }
        public int Status { get; set; }
        public int Posting_Action { get; set; }
        public int Status_Difference { get; set; }
        public decimal Qty__Difference { get; set; }
        public decimal Qty__Difference__base_ { get; set; }
        public decimal Difference { get; set; }
        public decimal Difference__base_ { get; set; }
        public string Reason_Code { get; set; }
        public string To_Location { get; set; }
        public string RF_ID { get; set; }
        public System.DateTime RF_Start_Time { get; set; }
        public System.DateTime RF_End_Time { get; set; }
        public string Serial_No_ { get; set; }
        public string Lot_No_ { get; set; }
        public System.DateTime Expiration_Date { get; set; }
    }
}
