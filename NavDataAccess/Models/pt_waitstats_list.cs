//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace NavDataAccess.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class pt_waitstats_list
    {
        public string sql_version { get; set; }
        public string wait_type { get; set; }
        public Nullable<bool> include { get; set; }
        public string description { get; set; }
    }
}
