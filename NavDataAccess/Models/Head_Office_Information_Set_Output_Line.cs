//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace NavDataAccess.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Head_Office_Information_Set_Output_Line
    {
        public byte[] timestamp { get; set; }
        public int Entry_No_ { get; set; }
        public int Line_No_ { get; set; }
        public int Line_Type { get; set; }
        public int Sorted_Position { get; set; }
        public string Grouping_Value_1 { get; set; }
        public int Grouping_ID_1 { get; set; }
        public int Grouping_Indent { get; set; }
        public int Grouped_Position { get; set; }
        public string Sorting_Value_1_Text { get; set; }
        public string Sorting_Value_2_Text { get; set; }
        public string Sorting_Value_3_Text { get; set; }
        public decimal Sorting_Value_1_Decimal { get; set; }
        public decimal Sorting_Value_2_Decimal { get; set; }
        public decimal Sorting_Value_3_Decimal { get; set; }
        public string Text_Field_1 { get; set; }
        public string Text_Field_2 { get; set; }
        public string Text_Field_3 { get; set; }
        public string Text_Field_4 { get; set; }
        public string Text_Field_5 { get; set; }
        public string Text_Field_6 { get; set; }
        public string Text_Field_7 { get; set; }
        public string Text_Field_8 { get; set; }
        public string Text_Field_9 { get; set; }
        public string Text_Field_10 { get; set; }
        public string Text_Field_11 { get; set; }
        public string Text_Field_12 { get; set; }
        public string Text_Field_13 { get; set; }
        public string Text_Field_14 { get; set; }
        public string Text_Field_15 { get; set; }
        public string Text_Field_16 { get; set; }
        public string Text_Field_17 { get; set; }
        public string Text_Field_18 { get; set; }
        public string Text_Field_19 { get; set; }
        public string Text_Field_20 { get; set; }
        public string Text_Field_21 { get; set; }
        public string Text_Field_22 { get; set; }
        public string Text_Field_23 { get; set; }
        public string Text_Field_24 { get; set; }
        public string Text_Field_25 { get; set; }
        public string Text_Field_26 { get; set; }
        public string Text_Field_27 { get; set; }
        public string Text_Field_28 { get; set; }
        public string Text_Field_29 { get; set; }
        public string Text_Field_30 { get; set; }
        public decimal Decimal_Field_1 { get; set; }
        public decimal Decimal_Field_2 { get; set; }
        public decimal Decimal_Field_3 { get; set; }
        public decimal Decimal_Field_4 { get; set; }
        public decimal Decimal_Field_5 { get; set; }
        public decimal Decimal_Field_6 { get; set; }
        public decimal Decimal_Field_7 { get; set; }
        public decimal Decimal_Field_8 { get; set; }
        public decimal Decimal_Field_9 { get; set; }
        public decimal Decimal_Field_10 { get; set; }
        public decimal Decimal_Field_11 { get; set; }
        public decimal Decimal_Field_12 { get; set; }
        public decimal Decimal_Field_13 { get; set; }
        public decimal Decimal_Field_14 { get; set; }
        public decimal Decimal_Field_15 { get; set; }
        public decimal Decimal_Field_16 { get; set; }
        public decimal Decimal_Field_17 { get; set; }
        public decimal Decimal_Field_18 { get; set; }
        public decimal Decimal_Field_19 { get; set; }
        public decimal Decimal_Field_20 { get; set; }
        public decimal Decimal_Field_21 { get; set; }
        public decimal Decimal_Field_22 { get; set; }
        public decimal Decimal_Field_23 { get; set; }
        public decimal Decimal_Field_24 { get; set; }
        public decimal Decimal_Field_25 { get; set; }
        public decimal Decimal_Field_26 { get; set; }
        public decimal Decimal_Field_27 { get; set; }
        public decimal Decimal_Field_28 { get; set; }
        public decimal Decimal_Field_29 { get; set; }
        public decimal Decimal_Field_30 { get; set; }
    }
}
