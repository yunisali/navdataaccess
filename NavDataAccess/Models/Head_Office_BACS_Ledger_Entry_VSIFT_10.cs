//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace NavDataAccess.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Head_Office_BACS_Ledger_Entry_VSIFT_10
    {
        public int Register_No_ { get; set; }
        public string Bal__Account_No_ { get; set; }
        public int Statement_Status { get; set; }
        public string Statement_No_ { get; set; }
        public Nullable<long> C_Cnt { get; set; }
        public Nullable<decimal> SUM_Amount { get; set; }
    }
}
