//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace NavDataAccess.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Head_Office_Preaction_Setup
    {
        public byte[] timestamp { get; set; }
        public int ID { get; set; }
        public int MainTableID { get; set; }
        public int Integer { get; set; }
        public string Text { get; set; }
        public string Code { get; set; }
        public decimal Decimal { get; set; }
        public int Option { get; set; }
        public byte Boolean { get; set; }
        public System.DateTime Date { get; set; }
        public System.DateTime Time { get; set; }
    }
}
