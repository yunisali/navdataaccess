//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace NavDataAccess.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Head_Office_MetaPack_Message_Line
    {
        public byte[] timestamp { get; set; }
        public string Document_No_ { get; set; }
        public int Document_Line_No_ { get; set; }
        public int Message_Type { get; set; }
        public string number { get; set; }
        public string countryOfOrigin { get; set; }
        public string fabricContent { get; set; }
        public string harmonisedProductCode { get; set; }
        public string miscellaneousInfo { get; set; }
        public string productCode { get; set; }
        public string productDescription { get; set; }
        public int productQuantity { get; set; }
        public string productTypeDescription { get; set; }
        public decimal totalProductValue { get; set; }
        public decimal unitProductWeight { get; set; }
        public byte Error { get; set; }
        public byte Sent { get; set; }
    }
}
