//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace NavDataAccess.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Head_Office_Test_Plan_Line
    {
        public byte[] timestamp { get; set; }
        public string Test_Plan_No_ { get; set; }
        public int Line_No_ { get; set; }
        public string Test_Task_No_ { get; set; }
        public int No__of_Iterations { get; set; }
    }
}
