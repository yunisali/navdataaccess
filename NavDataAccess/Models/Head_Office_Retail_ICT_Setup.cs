//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace NavDataAccess.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Head_Office_Retail_ICT_Setup
    {
        public byte[] timestamp { get; set; }
        public string Dist__Location { get; set; }
        public string ICT_BatchNo { get; set; }
        public string General_Journal { get; set; }
        public string Item_Journal { get; set; }
        public string Negative_Inv__Adjust__Group { get; set; }
        public string Positive_Inv__Adjust__Group { get; set; }
        public string Source_Code { get; set; }
        public byte Item_Journal_Dest__Unit_Cost { get; set; }
        public byte Debug { get; set; }
    }
}
