//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace NavDataAccess.Models
{
    using System;
    
    public partial class pt_sifttonavtablename_Result
    {
        public string SIFT_Table_Name { get; set; }
        public Nullable<int> NAV_Table_ID { get; set; }
        public string NAV_Table_Name { get; set; }
    }
}
