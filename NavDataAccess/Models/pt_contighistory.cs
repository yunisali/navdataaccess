//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace NavDataAccess.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class pt_contighistory
    {
        public int SampleNo { get; set; }
        public string ObjectName { get; set; }
        public short SchemaId { get; set; }
        public int ObjectId { get; set; }
        public string IndexName { get; set; }
        public int IndexId { get; set; }
        public Nullable<int> IndexDiff { get; set; }
        public Nullable<int> RowDiff { get; set; }
        public Nullable<int> OldFillFactor { get; set; }
        public Nullable<int> NewFillFactor { get; set; }
        public Nullable<int> AvgPageDensity { get; set; }
        public Nullable<int> AvgFreeBytes { get; set; }
        public Nullable<decimal> ScanDensity { get; set; }
        public Nullable<decimal> LogicalFrag { get; set; }
        public Nullable<decimal> ExtentFrag { get; set; }
        public Nullable<int> Extents { get; set; }
        public Nullable<int> ExtentSwitches { get; set; }
        public Nullable<int> BestCount { get; set; }
        public Nullable<int> ActualCount { get; set; }
        public Nullable<int> CountPages { get; set; }
        public Nullable<int> CountRows { get; set; }
        public Nullable<int> MinRecSize { get; set; }
        public Nullable<int> MaxRecSize { get; set; }
        public Nullable<int> AvgRecSize { get; set; }
        public Nullable<int> ForRecCount { get; set; }
        public Nullable<int> Lvl { get; set; }
    }
}
