//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace NavDataAccess.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Head_Office_Column_Layout
    {
        public byte[] timestamp { get; set; }
        public string Column_Layout_Name { get; set; }
        public int Line_No_ { get; set; }
        public string Column_No_ { get; set; }
        public string Column_Header { get; set; }
        public int Column_Type { get; set; }
        public int Ledger_Entry_Type { get; set; }
        public int Amount_Type { get; set; }
        public string Formula { get; set; }
        public string Comparison_Date_Formula { get; set; }
        public byte Show_Opposite_Sign { get; set; }
        public int Show { get; set; }
        public int Rounding_Factor { get; set; }
        public string Comparison_Period_Formula { get; set; }
        public string Business_Unit_Totaling { get; set; }
        public string Dimension_1_Totaling { get; set; }
        public string Dimension_2_Totaling { get; set; }
        public string Dimension_3_Totaling { get; set; }
        public string Dimension_4_Totaling { get; set; }
        public string Cost_Center_Totaling { get; set; }
        public string Cost_Object_Totaling { get; set; }
    }
}
