//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace NavDataAccess.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Head_Office_Dining_Tbl__Status_Chg__Timing
    {
        public byte[] timestamp { get; set; }
        public string Dining_Area_Profile_ID { get; set; }
        public int Dining_Table_No_ { get; set; }
        public string Status_Flow_ID { get; set; }
        public byte Change_Status { get; set; }
        public int Change_Dining_Table_Status { get; set; }
        public System.DateTime Date_Time_To_Change_Status { get; set; }
        public byte Delete_when_Dining_Tbl__Status { get; set; }
        public System.DateTime Date_Time_Created { get; set; }
    }
}
