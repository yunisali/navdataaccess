//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace NavDataAccess.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Head_Office_Forecourt_Store_Group_Price
    {
        public byte[] timestamp { get; set; }
        public int Entry_No_ { get; set; }
        public string Store_Group_Code { get; set; }
        public string Item_No_ { get; set; }
        public string Price_Group_Code { get; set; }
        public decimal New_Price { get; set; }
        public decimal Current_Price { get; set; }
        public decimal Price_Change { get; set; }
        public System.DateTime Valid_From { get; set; }
        public System.DateTime Valid_To { get; set; }
        public string Item_Description { get; set; }
        public string Price_Group_Name { get; set; }
    }
}
